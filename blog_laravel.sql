-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 22, 2016 at 12:50 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blog_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `bl_matches`
--

CREATE TABLE IF NOT EXISTS `bl_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_team` int(11) NOT NULL,
  `second_team` int(11) NOT NULL,
  `match_date` varchar(155) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `bl_matches`
--

INSERT INTO `bl_matches` (`id`, `first_team`, `second_team`, `match_date`, `created_at`, `updated_at`) VALUES
(1, 11, 9, '04/01/2016', '2016-03-21 23:47:43', '2016-03-22 05:17:43'),
(2, 6, 12, '04/01/2016', '2016-03-21 23:48:11', '2016-03-22 05:18:11'),
(3, 15, 4, '04/02/2016', '2016-03-21 23:48:43', '2016-03-22 05:18:43'),
(4, 8, 1, '04/02/2016', '2016-03-21 23:49:07', '2016-03-22 05:19:07'),
(5, 7, 5, '04/04/2016', '2016-03-21 23:49:43', '2016-03-22 05:19:43'),
(6, 14, 2, '04/04/2016', '2016-03-21 23:50:03', '2016-03-22 05:20:03'),
(7, 11, 13, '04/05/2016', '2016-03-21 23:50:40', '2016-03-22 05:20:40'),
(8, 6, 3, '04/05/2016', '2016-03-21 23:50:54', '2016-03-22 05:20:54'),
(9, 15, 10, '04/06/2016', '2016-03-21 23:51:24', '2016-03-22 05:21:24'),
(10, 9, 8, '04/06/2016', '2016-03-21 23:51:49', '2016-03-22 05:21:49'),
(11, 12, 14, '04/07/2016', '2016-03-21 23:52:04', '2016-03-22 05:22:04'),
(12, 4, 7, '04/07/2016', '2016-03-21 23:53:05', '2016-03-22 05:23:05'),
(13, 1, 13, '04/08/2016', '2016-03-21 23:53:15', '2016-03-22 05:23:15'),
(14, 2, 3, '04/08/2016', '2016-03-21 23:53:36', '2016-03-22 05:23:36'),
(15, 5, 10, '04/11/2016', '2016-03-21 23:56:10', '2016-03-22 05:26:10'),
(16, 11, 8, '04/11/2016', '2016-03-21 23:56:36', '2016-03-22 05:26:36'),
(17, 6, 14, '04/12/2016', '2016-03-21 23:56:58', '2016-03-22 05:26:58'),
(18, 15, 7, '04/12/2016', '2016-03-21 23:57:24', '2016-03-22 05:27:24'),
(19, 9, 1, '04/13/2016', '2016-03-21 23:57:45', '2016-03-22 05:27:45'),
(20, 12, 2, '04/13/2016', '2016-03-21 23:58:04', '2016-03-22 05:28:04'),
(21, 4, 5, '04/14/2016', '2016-03-21 23:58:28', '2016-03-22 05:28:28'),
(22, 8, 13, '04/14/2016', '2016-03-21 23:58:47', '2016-03-22 05:28:47'),
(23, 14, 3, '04/15/2016', '2016-03-21 23:59:03', '2016-03-22 05:29:03'),
(24, 7, 10, '04/15/2016', '2016-03-21 23:59:24', '2016-03-22 05:29:24'),
(25, 11, 1, '04/16/2016', '2016-03-21 23:59:54', '2016-03-22 05:29:54'),
(26, 6, 2, '04/16/2016', '2016-03-22 00:00:10', '2016-03-22 05:30:10'),
(27, 15, 5, '04/18/2016', '2016-03-22 00:00:24', '2016-03-22 05:30:24'),
(28, 9, 13, '04/18/2016', '2016-03-22 00:01:14', '2016-03-22 05:31:14'),
(29, 12, 3, '04/19/2016', '2016-03-22 00:01:37', '2016-03-22 05:31:37'),
(30, 4, 10, '04/19/2016', '2016-03-22 00:01:50', '2016-03-22 05:31:50');

-- --------------------------------------------------------

--
-- Table structure for table `bl_players`
--

CREATE TABLE IF NOT EXISTS `bl_players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_name` varchar(255) NOT NULL,
  `team` int(11) NOT NULL,
  `player_photo` varchar(255) NOT NULL,
  `player_type` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `bl_players`
--

INSERT INTO `bl_players` (`id`, `player_name`, `team`, `player_photo`, `player_type`, `created_at`, `updated_at`) VALUES
(1, 'Rahul Gupta', 11, 'd120b5f004a31e5e0da1eb47ba6b9ca3.jpg', 1, '2016-03-18 04:44:17', '2016-03-18 10:14:17'),
(2, 'Lokendra singh', 11, 'd9f45b0175d7c3256320d5e98c8b6e05.jpg', 1, '2016-03-18 04:44:34', '2016-03-18 10:14:34'),
(3, 'Harish Malviya', 11, 'cc0dfe169788b1b7b960c5510d04d50e.jpg', 1, '2016-03-18 04:50:09', '2016-03-18 10:20:09'),
(4, 'Bhaiyyalal Birle', 11, 'fa9666d78c04824fe6790c605eaf9863.jpg', 2, '2016-03-18 04:50:37', '2016-03-18 10:20:37'),
(5, 'Bharat Singh Kurmi', 11, '77cd432e9af4c448535db01ecdfb5652.jpg', 3, '2016-03-18 04:51:08', '2016-03-18 10:21:08'),
(6, 'Nitin Jangid', 11, 'e996daca881bf81baa5577b683de2220.jpg', 2, '2016-03-18 04:51:38', '2016-03-18 10:21:38'),
(7, 'Vishal Patidar', 11, 'e04f4cb59a1d34b1e2f3f88c8bf23fa1.jpg', 2, '2016-03-18 04:51:55', '2016-03-18 10:21:55'),
(8, 'Rishi Hardia', 11, 'f48e31a55b68cd05af023d8fec7cb027.jpg', 2, '2016-03-18 04:52:10', '2016-03-18 10:22:10'),
(9, 'Nitish Takzaria', 11, 'c213c13af6d1c8d6bbe30917020f95b2.jpg', 1, '2016-03-18 04:52:44', '2016-03-18 10:22:44'),
(10, 'Amit Kumar Sharma', 11, '4c30abbd52ce0627f49a1e0c74b91779.jpg', 2, '2016-03-18 04:53:05', '2016-03-18 10:23:05'),
(11, 'Chandravinod Sohagpure', 11, '09e89409ca37a0eaca5e814964c79ee7.jpg', 1, '2016-03-18 04:53:21', '2016-03-18 10:23:21'),
(12, 'Mohd Sameer Ansari', 11, '3552ffe4eeaca08a11b2b43fae1c9083.jpg', 2, '2016-03-18 04:53:59', '2016-03-18 10:23:59'),
(13, 'Saurabh Kadam', 11, '170d52a46015b65a895db6283f67bab8.jpg', 1, '2016-03-18 04:54:14', '2016-03-18 10:24:14'),
(14, 'Aman Raikwar', 11, 'bfae9624f6b337cee804de8838eda5ae.jpg', 2, '2016-03-18 04:54:38', '2016-03-18 10:24:38'),
(15, 'Ravindra Thakur', 11, 'ee393c1a769c61ac4b06029ebd407f63.jpg', 3, '2016-03-18 04:55:30', '2016-03-18 10:25:30'),
(16, 'Akeel Qureshi', 9, '590ee688da824da98dffbfb231e6910c.jpg', 1, '2016-03-19 02:53:34', '2016-03-19 08:23:34'),
(17, 'Ankit yadav', 9, 'df736a60743a1eb25d5c6f61701522c1.jpg', 1, '2016-03-19 02:54:02', '2016-03-19 08:24:02'),
(18, 'Chetan Gawade', 9, 'b5486238df53478dede3482d9b816076.jpg', 2, '2016-03-19 02:54:18', '2016-03-19 08:24:18'),
(19, 'Kamlesh Kumar', 9, 'e853c8618e45d7f935aa79749c37feb1.jpg', 2, '2016-03-19 02:54:31', '2016-03-19 08:24:31'),
(20, 'Pawan Purohit', 9, '96c538e200b6a8e6263ad059f08645b9.jpg', 3, '2016-03-19 02:54:46', '2016-03-19 08:24:46'),
(21, 'Mahendra Garg', 9, '0121c8bd08e9c4e7ac1d2f20bc5f53ab.jpg', 2, '2016-03-19 02:55:00', '2016-03-19 08:25:00'),
(22, 'Manish Prajapat', 9, 'e973c4af0ff7f7673bf54bc410eff5e7.jpg', 2, '2016-03-19 02:55:17', '2016-03-19 08:25:17'),
(23, 'Nitin Kumar Verma', 9, '2b528575493d803c528e0cbd0e8b0ea5.jpg', 3, '2016-03-19 02:55:31', '2016-03-19 08:25:31'),
(24, 'Aditya Rajak', 9, '7c13a0a9484018d6fcfe7e2c29b124cf.jpg', 1, '2016-03-19 02:55:43', '2016-03-19 08:25:43'),
(25, 'Sameer Talegaonkar', 9, '8e02708c0bb570905db055c0096889c7.jpg', 1, '2016-03-19 02:55:55', '2016-03-19 08:25:55'),
(26, 'Satyendra Singh Tomar', 9, 'f87e996c496c8dddc8cb30b394dc3b51.jpg', 2, '2016-03-19 02:56:13', '2016-03-19 08:26:13'),
(27, 'Shikhar Sharma', 9, 'd1825c61d7d4102b494ee096181d0aa6.jpg', 3, '2016-03-19 02:56:28', '2016-03-19 08:26:28'),
(28, 'Sourabh Upadhyay', 9, 'dbbf64e760325ac15b649a0c4da1a7bb.jpg', 1, '2016-03-19 02:56:41', '2016-03-19 08:26:41'),
(29, 'Vijay Mehta', 9, '530107a177cf1a33cd2f9f9971f029ef.jpg', 2, '2016-03-19 02:56:54', '2016-03-19 08:26:54'),
(30, 'Vivek Sahu', 9, '3103436128ce54a6291b9967a6ee3b68.jpg', 1, '2016-03-19 02:57:06', '2016-03-19 08:27:06'),
(31, 'Yogesh Prasad Dwivedi', 9, '51210fe8988ddb80a235ddbcbd840b9d.jpg', 1, '2016-03-19 02:57:25', '2016-03-19 08:27:25');

-- --------------------------------------------------------

--
-- Table structure for table `bl_player_type`
--

CREATE TABLE IF NOT EXISTS `bl_player_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_type` varchar(155) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bl_player_type`
--

INSERT INTO `bl_player_type` (`id`, `player_type`) VALUES
(1, 'All Rounder'),
(2, 'BatsMan'),
(3, 'Bowler');

-- --------------------------------------------------------

--
-- Table structure for table `bl_session`
--

CREATE TABLE IF NOT EXISTS `bl_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `bl_session`
--

INSERT INTO `bl_session` (`id`, `session_name`, `created_at`, `updated_at`) VALUES
(1, '2016', '2016-03-18 04:32:39', '2016-03-18 10:02:39');

-- --------------------------------------------------------

--
-- Table structure for table `bl_team`
--

CREATE TABLE IF NOT EXISTS `bl_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(255) NOT NULL,
  `team_logo` varchar(255) NOT NULL,
  `team_manager` varchar(255) NOT NULL,
  `team_coach` varchar(255) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `_token` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `bl_team`
--

INSERT INTO `bl_team` (`id`, `team_name`, `team_logo`, `team_manager`, `team_coach`, `status`, `created_at`, `updated_at`, `_token`) VALUES
(1, 'Blaster Daredevils ', '8138444aa9e90c8024cc84779fcd3189.jpg', 'Shashank Shrivastava', 'Vivek Kumar Singh', 0, '2016-03-18 04:37:47', '2016-03-18 10:07:47', ''),
(2, 'Challenging All Rounders', '6443e9f342d2a6e01c4d039933be023f.jpg', 'Dilip Baherwani', 'Jayant Vishway', 0, '2016-03-18 04:38:33', '2016-03-18 10:08:33', ''),
(3, 'Champions ', 'cb1f51ca6726a5266fba06ed120c0f90.jpg', 'Varun Mishra', 'Vindesh Mohariya', 0, '2016-03-18 04:38:57', '2016-03-18 10:08:57', ''),
(4, 'CISIN EAGLES', '386bdff7a17db51c4599675ddf746b7a.jpg', 'Arun Sharma', 'Durgesh Sharma', 0, '2016-03-18 04:39:39', '2016-03-18 10:09:39', ''),
(5, 'CMS-11', '3228f95ccfa7588c2ff8b11f7e23f460.jpg', 'Kamlesh Rewapati', 'Devdutt Sharma', 0, '2016-03-18 04:40:02', '2016-03-18 10:10:02', ''),
(6, 'Creative Ninjas ', 'e6fbff8442364ad23b316822d029e1ff.jpg', 'Sandeep  Sharma', 'Shubham Doliya', 0, '2016-03-18 04:40:29', '2016-03-18 10:10:29', ''),
(7, 'Furious Dragons ', '8e6379620249ab128ee2a993b5797f37.jpg', 'Ajay Sharma', 'Akhilesh Tiwari', 0, '2016-03-18 04:40:43', '2016-03-18 10:10:43', ''),
(8, 'Maverick Lions ', '3715f97f9c0597c34ebb0d45cc609c04.jpg', 'Ruchir Chandratre', 'Ritesh Bhatia', 0, '2016-03-18 04:41:06', '2016-03-18 10:11:06', ''),
(9, 'Royal Strikers', '9c00e3c459bb61c208ac913216fcbf92.jpg', 'Akeel Qureshi', 'Ankit yadav', 0, '2016-03-18 04:41:33', '2016-03-18 10:11:33', ''),
(10, 'The Scoring Willows ', 'd93b4dce45f35a0578412db8fbffd2fa.jpg', 'Radheshyam Dhangar', 'Kshitij Shukla ', 0, '2016-03-18 04:41:46', '2016-03-18 10:11:46', ''),
(11, 'Smartian Tuskers', '774f40b23149c9933afaa007101c40f9.jpg', 'Ravindra Thakur', 'Aman Raikwar', 0, '2016-03-18 04:42:02', '2016-03-18 10:12:02', ''),
(12, 'Smashers', '7c7868613601233d9b56202578ad3e8a.jpg', 'Mahendra Solanki', 'Nitish Shrivastava', 0, '2016-03-18 04:42:31', '2016-03-18 10:12:31', ''),
(13, 'Spartans ', '04881e410bcdf773fb5622112dc5567b.jpg', 'Yogesh Raghuvanshi', 'Ashish Udasi', 0, '2016-03-18 04:42:51', '2016-03-18 10:12:51', ''),
(14, 'Sud Warriors', '02dc37374322619c34058e9cdefc06c2.jpg', 'Sudhansu Dubey', 'Atul khare ', 0, '2016-03-18 04:43:07', '2016-03-18 10:13:07', ''),
(15, 'Web Riders', '0e01d2a530b57eee9609cbf079368a84.jpg', 'Deepak Rathore', 'Pratik Raghuwanshi', 0, '2016-03-18 04:43:21', '2016-03-18 10:13:21', '');

-- --------------------------------------------------------

--
-- Table structure for table `bl_users`
--

CREATE TABLE IF NOT EXISTS `bl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(150) NOT NULL,
  `user_email` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

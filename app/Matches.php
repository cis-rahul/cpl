<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matches extends Model
{
   protected $table = 'bl_matches';
   protected $fillable = ['id','first_team','second_team','match_date'];
}

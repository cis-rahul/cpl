<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
//front
Route::get('/', ['uses'=>'HomeController@index', 'as' => 'admin.homepage']);

Route::get('cpl-player', ['uses'=>'HomeController@cplPlayers', 'as' => 'front.cplplayer']);
Route::get('cpl-player-detail/{id}', ['uses'=>'HomeController@cplPlayersDetail', 'as' => 'front.cplplayerdetail']);
Route::get('cpl-match-schedule', ['uses'=>'HomeController@cplSchedule', 'as' => 'front.cplschedule']);
Route::get('cpl-top-player-list', ['uses'=>'HomeController@cplTopplayers', 'as' => 'front.cpltopplayers']);
Route::get('cpl-score-card/{id}', ['uses'=>'HomeController@cplScorecard', 'as' => 'front.cplscorecard']);



//admin
Route::get('/dashboard', ['uses'=>'LoginController@index', 'as' => 'admin.loginpage']);

//login admin 
Route::get('login-page', ['uses'=>'LoginController@loginView', 'as' => 'admin.loginpage']);
Route::post('login', ['uses'=>'LoginController@auth', 'as' => 'admin.checklogin']);
Route::get('logout', ['uses'=>'LoginController@logout', 'as' => 'admin.logout']);


//admin end 
Route::get('users', 'UserController@create');
Route::get('dashboard', ['uses'=>'DashboardController@index', 'as' => 'admin.dashbaord']);

//seasion
Route::get('session-add', ['uses'=>'SessionController@index', 'as' => 'admin.session']);
Route::post('session', ['uses'=>'SessionController@Add', 'as' => 'admin.addsession']);
Route::get('session-list', ['uses'=>'SessionController@Lists', 'as' => 'admin.allsession']);
Route::post('session-update', ['uses'=>'SessionController@Update', 'as' => 'admin.updatesession']);
Route::get('session-edit/{id}', ['uses'=>'SessionController@Edit', 'as' => 'admin.editsession']);
Route::get('session-delete/{id}', ['uses'=>'SessionController@Delete', 'as' => 'admin.deletesession']);

//team
Route::get('team', ['uses'=>'TeamController@index', 'as' => 'admin.index']);
Route::post('team-add', ['uses'=>'TeamController@Add', 'as' => 'admin.teamadd']);
Route::get('team-list', ['uses'=>'TeamController@Lists', 'as' => 'admin.allteams']);
Route::get('team-edit/{id}', ['uses'=>'TeamController@Edit', 'as' => 'admin.editeam']);
Route::post('team-update', ['uses'=>'TeamController@Update', 'as' => 'admin.updateam']);
Route::get('team-delete/{id}', ['uses'=>'TeamController@Delete', 'as' => 'admin.deleteam']);

//player
Route::get('player', ['uses'=>'PlayerController@index', 'as' => 'admin.player']);
Route::post('player-add', ['uses'=>'PlayerController@Add', 'as' => 'admin.playeradd']);
Route::get('player-list', ['uses'=>'PlayerController@Lists', 'as' => 'admin.allplayers']);
Route::get('player-edit/{id}', ['uses'=>'PlayerController@Edit', 'as' => 'admin.editplayer']);
Route::post('player-update', ['uses'=>'PlayerController@Update', 'as' => 'admin.updateplayer']);
Route::get('player-delete/{id}', ['uses'=>'PlayerController@Delete', 'as' => 'admin.deleplayer']);

//matches 
Route::get('match-entry', ['uses'=>'LatestmatchControll@index', 'as' => 'admin.addmatch']);
Route::post('match-add', ['uses'=>'LatestmatchControll@create', 'as' => 'admin.matchadd']);
Route::get('match-all', ['uses'=>'LatestmatchControll@show', 'as' => 'admin.allmatches']);
Route::get('match-list/{id}', ['uses'=>'LatestmatchControll@edit', 'as' => 'admin.editmatch']);
Route::post('match-update', ['uses'=>'LatestmatchControll@Update', 'as' => 'admin.matchupdate']);
Route::get('match-delete/{id}', ['uses'=>'LatestmatchControll@destroy', 'as' => 'admin.deletematch']);
Route::post('matchstatus', ['uses'=>'LatestmatchControll@matchStatus', 'as' => 'admin.matchstatus']);


//umpires 
Route::get('umpires-entry', ['uses'=>'UmpiresController@index', 'as' => 'admin.addump']);
Route::post('umpires-add', ['uses'=>'UmpiresController@create', 'as' => 'admin.umpadd']);
Route::get('umpire-all', ['uses'=>'UmpiresController@show', 'as' => 'admin.allump']);
Route::get('ump-list/{id}', ['uses'=>'UmpiresController@edit', 'as' => 'admin.editump']);
Route::post('ump-update', ['uses'=>'UmpiresController@Update', 'as' => 'admin.updateump']);
Route::get('ump-delete/{id}', ['uses'=>'UmpiresController@destroy', 'as' => 'admin.deleteump']);

//score result 
Route::get('enter-score/{x}/{y}/{z}', ['uses'=>'ScoreController@index', 'as' => 'admin.addscore']);
Route::post('first-match-data', ['uses'=>'ScoreController@AddRecords', 'as' => 'admin.firstmatchcontent']);
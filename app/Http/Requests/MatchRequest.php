<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MatchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_name_first'   => 'required',
            'team_name_first'   => 'required',
            'team_name_second'  => 'required',
            'datepicker'        => 'required'
        ];
    }
}

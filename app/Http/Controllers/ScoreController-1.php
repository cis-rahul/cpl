<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\RequestFirstTeamData;
use App\Http\Controllers\Controller;
use DB;
use View;
use Redirect;

class ScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ftm_id,$stm_id)
    {
        $ftm_player = array();
        $stm_player = array();
        //get first team player
        $f_players = DB::select('select id,player_name from bl_players where team = :team', ['team' => $ftm_id]);
        
        foreach($f_players as $fp){
            $ftm_player[] = $fp->player_name .'_'.$fp->id;
        }
        $player_data = explode('_', $ftm_player);
        print_r($player_data);die;
        //get second team player
        $s_players = DB::select('select player_name from bl_players where team = :team', ['team' => $stm_id]);
        
        foreach($s_players as $sp){
            $stm_player[] = $sp->player_name;
        }

        //to get team name 
        $ftm  = DB::select('select team_name from bl_team where id = :id', ['id' => $ftm_id]);
        $first_team = $ftm[0]->team_name;

        $stm  = DB::select('select team_name from bl_team where id = :id', ['id' => $stm_id]);
        $second_team = $stm[0]->team_name; 
        return View::make('admin.enterscore', array('first_team_players' => $ftm_player , 'second_team_players' => $stm_player , 'first_team_name' => $first_team , 'second_team_name' => $second_team));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function AddFirstMatch(RequestFirstTeamData $request)
    {
    echo $request->get('runs');die;
    }



    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

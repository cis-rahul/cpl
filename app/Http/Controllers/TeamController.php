<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeamFormRequest;
use App\Http\Requests\TeamFormRequest_update;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Team;
use Response;
use View;
use Request;
use Input;
use Redirect;
use Session;
use DB;
use Files;
use Intervention\Image\Image as Image;

class TeamController extends Controller {

    public function index() {
        return view('admin.addteam');
    }

    public function Add(TeamFormRequest $request) {

        $file = Input::file('team_logo');
        $input = array('image' => $file);
        $destinationPath = 'uploads';
        $filename = md5(microtime() . $file->getClientOriginalName()) . "." . $file->getClientOriginalExtension();
        Input::file('team_logo')->move($destinationPath, $filename); 

        $team_data = new Team(array( 'team_logo'  => $filename));

        $team_data = new Team(array(
              'team_name'    => $request->get('team_name'),
              'team_manager' => $request->get('team_manager'),
              'team_coach'   => $request->get('team_coach'),
              'team_logo'    => $filename
        ));
        
        $team_data->save();

        return Redirect::to(route('admin.allteams'))->with('message', 'Team successfully added');
    }

    public function Lists() {

        $team_data = DB::table('bl_team')->get();
        return view('admin.listeams')->with('team_data', $team_data);
    }

    public function Edit(Team $team , $id) {
         
        $selected_team = Team::find($id);
        return view('admin.editeam')->with('selected_team', $selected_team);
    }

    public function Update(TeamFormRequest_update $request) {
        
        $update_teamdata = array();        
        $id = $request->team_id;
        
         $update_teamdata = array(
                    'team_name'    => $request->team_name, 
                    'team_manager' => $request->team_manager ,
                    'team_coach'   => $request->team_coach
                ); 

        $file = Input::file('team_logo');

        if($file){
            
            //get existing image
            $team_re = DB::table('bl_team')->where('id', $id)->first();
            $old_img = $team_re->team_logo;

            $destinationPath = 'uploads';
            $input = array('image' => $file);

            $filename = md5(microtime() . $file->getClientOriginalName()) . "." . $file->getClientOriginalExtension();
            Input::file('team_logo')->move($destinationPath, $filename);
                $update_teamdata['team_logo'] = $filename;
            }  

            $update_recored = DB::table('bl_team')
                ->where('id', $id)
                ->update($update_teamdata);

                if($update_recored){
                    if(isset($old_img)){
                         $path = base_path('/uploads/'.$old_img);
                             if(file_exists($path)){
                             unlink($path);    
                        }
                    }
                }
                
                    
        return Redirect::to(route('admin.allteams'))->with('message', 'Record successfully Updated');
    }
    
    public function Delete($id){ 

        DB::table('bl_team')->where('id',$id)->delete();
        
        return Redirect::to(route('admin.allteams'))->with('message', 'Record successfully Deleted');
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UmpireFormRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use App\Umpires;
use Illuminate\Support\Facades\Validator;
use App\Player;
use Response;
use View; 
use Input; 
use Session;
use DB;

class UmpiresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $team_data = DB::table('bl_team')->select('id','team_name')->get();
        $teams_name =array();
        
       $teams_name[''] = "Select Team";
         foreach($team_data as $data) {
                $teams_name[$data->id] = $data->team_name;
        }

        return View::make('admin.addumpire', [
                        'team_data' => $teams_name
                    ]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(UmpireFormRequest $request)
    {

        $ump_data = new Umpires(array(
              'ump_name'      => $request->get('umpire_name'),
              'ump_team_name' => $request->get('umpire_team_name')
        ));
        
        $ump_data->save();

        return Redirect::to(route('admin.allump'))->with('message', 'Umpire successfully added');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $ump_data = DB::table('bl_umpires')
            ->join('bl_team', 'bl_umpires.ump_team_name', '=', 'bl_team.id')
            ->select('bl_umpires.id','bl_umpires.ump_name','bl_umpires.created_at' , 'bl_team.team_name')
            ->get();        
        return view('admin.listumpies')->with('ump_data', $ump_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team_data = DB::table('bl_team')->select('id','team_name')->get();
        $teams_name =array();
        
        $teams_name[''] = "Select Team";
         foreach($team_data as $data) {
                $teams_name[$data->id] = $data->team_name;
        }

        
        $selected_data = Umpires::find($id);

        return View::make('admin.editump', [
                        'team_data'       => $teams_name,
                        'selected_data'   => $selected_data
                    ]);
    }

    
    public function update(UmpireFormRequest $request)
    { 
        $update_umpdata = array(
                    'ump_name'      => $request->umpire_name, 
                    'ump_team_name' => $request->umpire_team_name 
                );


        $id = $request->ump_id;
        $update_recored = DB::table('bl_umpires')
                ->where('id', $id)
                ->update($update_umpdata);
                  
        return Redirect::to(route('admin.allump'))->with('message', 'Record successfully Updated');   
    }

    public function destroy($id)
    {
      
      $ump_record = DB::table('bl_umpires')->where('id',$id)->delete();
      if($ump_record){
         return Redirect::to(route('admin.allump'))->with('message', 'Record successfully Deleted');
      }else{
         return Redirect::to(route('admin.allump'))->with('message', 'Record Not Deleted');   
      }
     
    }
}

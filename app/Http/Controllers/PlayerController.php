<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayerFormRequest;
use App\Http\Requests\PlayerFormRequest_update;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\Player;
use Response;
use View;
use Request;
use Input;
use Redirect;
use Session;
use DB;
use Files;
use Intervention\Image\Image as Image;

class PlayerController extends Controller {

    public function index() {

        $team_data = DB::table('bl_team')->select('id','team_name')->get();
        $player_data = DB::table('bl_player_type')->get();


        $teams_name =array();
        $player_type =array();

        $teams_name[''] = "Select Team";
         foreach($team_data as $data) {
                $teams_name[$data->id] = $data->team_name;
        }

        $player_type[''] = "Select Player";
         foreach($player_data as $data) {
                $player_type[$data->id] = $data->player_type;
        }


        return View::make('admin.addplayer', [
                        'team_data' => $teams_name,
                        'player_type' => $player_type
                    ]);
        }

    public function Add(PlayerFormRequest $request) {


        $file = Input::file('player_photo');
        $input = array('image' => $file);
        $destinationPath = 'uploads';
        $filename = md5(microtime() . $file->getClientOriginalName()) . "." . $file->getClientOriginalExtension();
        Input::file('player_photo')->move($destinationPath, $filename); 

        $team_data = new Player(array( 'player_photo'  => $filename));

        $team_data = new Player(array(
              'player_name'    => $request->get('player_name'),
              'team'           => $request->get('team_name'),
              'player_type'    => $request->get('player_type'),
              'player_photo'   => $filename
        ));
        
        $team_data->save();

        return Redirect::to(route('admin.allplayers'))->with('message', 'Player successfully added');
    }

    public function Lists() {

        //$player_data = DB::table('bl_players')->get();
          $player_data = DB::table('bl_players')
            ->join('bl_team', 'bl_players.team', '=', 'bl_team.id')
            ->join('bl_player_type', 'bl_players.player_type', '=', 'bl_player_type.id')
            ->select('bl_players.id', 'bl_players.player_name','bl_players.player_photo','bl_players.created_at','bl_team.team_name','bl_player_type.player_type')
            ->get(); 

        return view('admin.listplayers')->with('player_data', $player_data);
    }

    public function Edit(Player $player , $id) {
         
        

        $team_data = DB::table('bl_team')->select('id','team_name')->get();
        $player_data = DB::table('bl_player_type')->get();


        $teams_name =array();
        $player_type =array();

        $teams_name[''] = "Select Team";
         foreach($team_data as $data) {
                $teams_name[$data->id] = $data->team_name;
        }

        $player_type[''] = "Select Player";
         foreach($player_data as $data) {
                $player_type[$data->id] = $data->player_type;
        }

        $selected_data = Player::find($id);

        // $selected_player = array('id' => $selected_data->id ,
        //                          'player_name' => $selected_data->player_name,
        //                          'team' => $selected_data->team,
        //                          'player_photo' => $selected_data->player_photo,
        //                          'player_type' => $selected_data->player_type
        //                         );
       //print_r($selected_data);die;
        return View::make('admin.editplayer', [
                        'team_data'       => $teams_name,
                        'selected_player' => $selected_data,
                        'player_type'     => $player_type
                    ]);
    }

    public function Update(PlayerFormRequest_update $request) {
        
        $update_playerdata = array();        
        $id = $request->player_id;
        
        $update_playerdata = array(
                    'player_name'    => $request->player_name, 
                    'team'         => $request->team_name ,
                    'player_type'  => $request->player_type
                ); 

        $file = Input::file('player_photo');

        if($file){
            
            //get existing image
            $team_re = DB::table('bl_players')->where('id', $id)->first();
            $old_img = $team_re->player_photo;
            $destinationPath = 'uploads';
            $input = array('image' => $file);

            $filename = md5(microtime() . $file->getClientOriginalName()) . "." . $file->getClientOriginalExtension();
            Input::file('player_photo')->move($destinationPath, $filename);
                $update_playerdata['player_photo'] = $filename;
            }  

            $update_recored = DB::table('bl_players')
                ->where('id', $id)
                ->update($update_playerdata);

                if($update_recored){
                    if(isset($old_img)){
                         $path = base_path('/uploads/'.$old_img);
                             if(file_exists($path)){
                             unlink($path);    
                        }
                    }
                }
                
                    
        return Redirect::to(route('admin.allplayers'))->with('message', 'Record successfully Updated');
    }
    
    public function Delete($id){ 


         $player_record = DB::table('bl_players')->where('id', $id)->first();
                $old_img = $player_record->player_photo;
                if(isset($old_img)){
                     $path = base_path('/uploads/'.$old_img);
                         if(file_exists($path)){
                         unlink($path);    
            }
        }

        $delete_player = DB::table('bl_players')->where('id',$id)->delete();
       
           
        
        
        return Redirect::to(route('admin.allplayers'))->with('message', 'Record successfully Deleted');
    }

}

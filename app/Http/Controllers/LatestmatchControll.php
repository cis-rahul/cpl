<?php

namespace App\Http\Controllers;

use App\Http\Requests\MatchRequest;
use App\Http\Requests\MatchURequest;
use App\Http\Requests;
use App\Matches;
use DB;
use View;
use Redirect;
use App\Http\Controllers\Controller;

class LatestmatchControll extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        //die("dfsdfsdfsdfdfdsfdsf");
    }
    public function index()
    {
        $team_data = DB::table('bl_team')->select('id','team_name')->get();
        $teams_name =array();
        
        $teams_name[''] = "Select Team";
         foreach($team_data as $data) {
                $teams_name[$data->id] = $data->team_name;
        }

        return View::make('admin.matchentry', [
                        'team_data' => $teams_name
                    ]);
        }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(MatchRequest $request)
    {
        $match_data = new Matches(array(
              'first_team'    => $request->get('team_name_first'),
              'second_team'   => $request->get('team_name_second'),
              'match_date'    => $request->get('datepicker')
        ));
        
        $match_data->save();

        return Redirect::to(route('admin.allmatches'))->with('message', 'Match successfully added');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
         //$player_data = DB::table('bl_players')->get();
          $match_data = DB::table('bl_matches as m')
            ->join('bl_team as t1', 'm.first_team', '=', 't1.id')
            ->join('bl_team as t2', 'm.second_team', '=', 't2.id')
            ->select('t1.team_name as first_team','t2.team_name as second_team','m.id','m.match_date','m.first_team as ft_id','m.second_team as fs_id')
            ->get(); 
            return view('admin.listmatches')->with('match_data', $match_data); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $selected_match = Matches::find($id);

        $team_data = DB::table('bl_team')->select('id','team_name')->get();
        $teams_name =array();
        
        $teams_name[''] = "Select Team";
         foreach($team_data as $data) {
                $teams_name[$data->id] = $data->team_name;
        }


        return View::make('admin.editmatch', [
                        'team_data'  => $teams_name,
                        'match_data' => $selected_match
                    ]);

    }

    public function update(MatchRequest $request)
    {
        $match_id =  $request->match_id;
        $update_matchdata = array(
                    'first_team'    => $request->get('team_name_first'),
                    'second_team'   => $request->get('team_name_second'),
                    'match_date'    => $request->get('datepicker')
                ); 
         $update_recored = DB::table('bl_matches')
                ->where('id', $match_id)
                ->update($update_matchdata);

        return Redirect::to(route('admin.allmatches'))->with('message', 'Record successfully Updated');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    
    public function destroy($id)
    {   
       $delete_match = DB::table('bl_matches')->where('id',$id)->delete();
       return Redirect::to(route('admin.allmatches'))->with('message', 'Record successfully Deleted');
    }
    
    public function matchStatus(MatchURequest $request){        
        $match_id = $request->input('match_id');
        if(!empty($match_id)){
            $first_umpire = $request->input('first-umpires');
            $second_umpire = $request->input('second-umpires');
            $match_result = $request->input('match_result');
            $first_team_id = $request->input('first_team_id');
            $team_run = $request->input('team_run');
            $team_over = $request->input('team_over');
            $second_team_run = $request->input('second_team_run');
            $second_team_over = $request->input('second_team_over');
            $second_team_id = $request->input('second_team_id');
            $model = Matches::find($match_id);
            $model->first_umpire = $first_umpire;
            $model->first_team_run = $team_run[$first_team_id];
            $model->second_team_run = $team_run[$second_team_id];
            $model->first_team_over = $team_over[$first_team_id];
            $model->second_team_over = $team_over[$second_team_id];
            $model->result = $match_result;
            $model->second_umpire = $second_umpire;
            $model->save();        
        }
        $first_team_id = trim($first_team_id);
        $second_team_id = trim($second_team_id);
        $match_id = trim($match_id);
        return Redirect::to(route('admin.addscore', array('ftm'=>$first_team_id,'stm'=>$second_team_id,'match_id'=>$match_id)));
    }
}

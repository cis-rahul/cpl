<?php

namespace App\Http\Controllers;

use App\Http\Requests\FriendFormRequest;
use Illuminate\Routing\Controller;
use App\FriendForm;
use Response;
use View;
use Request;
use Input;
use Redirect;
use Session;
use DB;

class SessionController extends Controller {

    public function index() {
        return view('admin.addsession');
    }

    public function Add(FriendFormRequest $request, FriendForm $session) {

        //print_r($request);
        $session->session_name = Input::get('session_name');
        $session->save();
        return Redirect::to(route('admin.allsession'))->with('message', 'Season successfully added');
    }

    public function Lists(FriendForm $session) {

        $session_data = DB::table('bl_session')->get();
        return view('admin.listsession')->with('session_data', $session_data);
    }

    public function Edit(FriendForm $session, $id) {

        $selected_session = FriendForm::find($id);
        return view('admin.editsession')->with('selected_session', $selected_session);
    }

    public function Update(FriendFormRequest $request) {
        
        $id = $request->session_id;
        $session_name = $request->session_name;
        DB::table('bl_session')
                ->where('id', $id)
                ->update(['session_name' => $session_name]);

        return Redirect::to(route('admin.allsession'))->with('message', 'Season successfully Updated');
    }
    
    public function Delete($id){
        
        DB::table('bl_session')->where('id',$id)->delete();
        
        return Redirect::to(route('admin.allsession'))->with('message', 'Season successfully Deleted');
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\MatchRequest;
use App\Http\Requests;
use App\Matches;
use DB;
use View;
use Redirect;
use App\Http\Controllers\Controller;
use DateTime;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        //die("Hi");
        
    }
    
    public function index()
    {        
        $now =  date("m/d/Y");
        $match_data = DB::table('bl_matches')            
            ->join('bl_team as t1', 'bl_matches.first_team', '=', 't1.id')
            ->join('bl_team as t2', 'bl_matches.second_team', '=', 't2.id')
            ->select('bl_matches.id as match_id','bl_matches.match_date','t1.id as t1_id' , 't2.id as t2_id' , 't1.team_logo as t1_team_logo','t2.team_logo as t2_team_logo','t1.team_name as t1_team_name','t2.team_name as t2_team_name')
            ->where('match_date', $now)
            ->get();        
        $top_five_bastman = DB::table('bl_records')
            ->select(DB::raw('sum(bl_records.run) as total_run, sum(bl_records.ball) as total_ball, bl_records.player_id, bl_players.player_name, bl_team.team_logo'))
            ->join('bl_players', 'bl_players.id', '=', 'bl_records.player_id')
            ->join('bl_team', 'bl_team.id', '=', 'bl_records.team_id')    
            ->groupBy('player_id')
            ->orderBy('total_run', 'desc')
            ->orderBy('total_ball', 'asc')    
            ->take('5')   
            ->get();
        $top_five_bowler = DB::table('bl_records')
            ->select(DB::raw('sum(bl_records.wicket) as total_wicket, sum(bl_records.given_run) as total_given_run, sum(bl_records.over) as total_over, bl_players.player_name, bl_team.team_logo'))
            ->join('bl_players', 'bl_players.id', '=', 'bl_records.player_id')
            ->join('bl_team', 'bl_team.id', '=', 'bl_records.team_id')    
            ->groupBy('player_id')
            ->orderBy('total_wicket', 'desc')
            ->orderBy('total_given_run', 'asc')
            ->take('5')   
            ->get();
        
        $matchesdata = DB::table('bl_matches')            
            ->join('bl_team as t1', 'bl_matches.first_team', '=', 't1.id')
            ->join('bl_team as t2', 'bl_matches.second_team', '=', 't2.id')
            ->select('bl_matches.id as match_id','bl_matches.match_date','t1.id as t1_id' , 't2.id as t2_id' , 't1.team_logo as t1_team_logo','t2.team_logo as t2_team_logo','t1.team_name as t1_team_name','t2.team_name as t2_team_name', 'bl_matches.first_team_run','bl_matches.first_team_over', 'bl_matches.second_team_run','bl_matches.second_team_over','bl_matches.first_team','bl_matches.second_team', 'bl_matches.result')
            ->get();         
        $top_teams = array();
        if(!empty($matchesdata)){
            foreach($matchesdata as $objMatch){                 
                if(!empty($objMatch->first_team_run)){
                    
//                    $per = ($first_over_arr[1] / 6 * 100);
//                    $colper = (10 * $per)/100;
//                    $removedot = explode(".", $colper);
//                    $first_team_over_after_call = $first_over_arr[0] . "." . $removedot[0];
//                    
//                    $second_over_arr = explode('.', $objMatch->second_team_over);
//                    $per1 = ($second_over_arr[1] / 6 * 100);
//                    $colper1 = (10 * $per1)/100;
//                    $removedot1 = explode(".", $colper1);
//                    $second_team_over_after_call = $second_over_arr[0] . "." . $removedot1[0];
                    
                    $first_over_arr = explode('.', $objMatch->first_team_over);//                    
                    $first_team_over_after_call = $first_over_arr[0]  + ($first_over_arr[1] / 6);
                    
                    $second_over_arr = explode('.', $objMatch->second_team_over);
                    $second_team_over_after_call = $second_over_arr[0] + ($second_over_arr[1] / 6) ;
                    
                    
                    // $first_team_avg = ($objMatch->first_team_run/$first_team_over_after_call) - ($objMatch->second_team_run/$second_team_over_after_call);
                    // $second_team_avg = ($objMatch->second_team_run/$second_team_over_after_call) - ($objMatch->first_team_run/$first_team_over_after_call);
                    $top_teams[$objMatch->first_team]['first_team']['run'][] = $objMatch->first_team_run;
                    $top_teams[$objMatch->first_team]['first_team']['over'][] = $first_team_over_after_call;
                    $top_teams[$objMatch->first_team]['second_team']['run'][] = $objMatch->second_team_run;
                    $top_teams[$objMatch->first_team]['second_team']['over'][] = $second_team_over_after_call;
                    $top_teams[$objMatch->second_team]['first_team']['run'][] = $objMatch->second_team_run;
                    $top_teams[$objMatch->second_team]['first_team']['over'][] = $second_team_over_after_call;
                    $top_teams[$objMatch->second_team]['second_team']['run'][] = $objMatch->first_team_run;
                    $top_teams[$objMatch->second_team]['second_team']['over'][] = $first_team_over_after_call;
                }
              
            }            
        }
        $top_team_array = array();
        if(!empty($top_teams)){
            foreach($top_teams as $key=>$objteams){                
                $length = count($objteams['first_team']['run']);
                $i = 0;
                $total_run_first = 0;
                $total_over_first = 0;
                $total_run_second = 0;
                $total_over_second = 0;
                while($length > $i){
                    $total_run_first = $total_run_first + $objteams['first_team']['run'][$i];
                    $total_over_first = $total_over_first + $objteams['first_team']['over'][$i];
                    $total_run_second = $total_run_second + $objteams['second_team']['run'][$i];
                    $total_over_second = $total_over_second + $objteams['second_team']['over'][$i];
                    $i++;
                } 
                $total_first =  $total_run_first / $total_over_first ;           
                $total_second =  $total_run_second / $total_over_second ; 
                $total =   $total_first - $total_second;
                $top_team_array[$key] = round($total, 3); 
            }
        }
        $finalMatchPointArr = array();
        if(!empty($top_team_array)){
            foreach($top_team_array as $key=>$finalObj){                
                $totalwin = DB::table('bl_matches') 
                ->join('bl_team', 'bl_team.id', '=', 'bl_matches.result')
                ->select('bl_team.team_name', 'bl_team.team_logo')
                ->where('bl_matches.result', '=', $key)        
                ->get();                
                $point = (count($totalwin) * 2);
                $finalMatchPointArr[$key]['point'] = $point;
                $finalMatchPointArr[$key]['avg'] = $finalObj;
                $finalMatchPointArr[$key]['team_name'] = isset($totalwin[0]->team_name)?$totalwin[0]->team_name:"";
                $finalMatchPointArr[$key]['team_logo'] = isset($totalwin[0]->team_logo)?$totalwin[0]->team_logo:"";
            }            
        }
        $column1 = array();
        $column2 = array();
        $column3 = array();
        if(!empty($finalMatchPointArr)){
            foreach($finalMatchPointArr as $key => $obj){
                    $column1[$key] = $obj['point'];
                    $column2[$key] = $obj['avg'];
                    $column3[$key] = $obj['team_name'];
            }
        }
        array_multisort($column1, SORT_DESC, $column2, SORT_DESC, $finalMatchPointArr);
        return View::make('front.index')->with(array('match_data'=>$match_data, 'top_five_bastman'=>$top_five_bastman,'top_five_bowler'=>$top_five_bowler, 'top_five_team'=>$finalMatchPointArr));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cplPlayers()
    {
        $all_data = DB::table('bl_team')
            ->join('bl_players', 'bl_team.id', '=', 'bl_players.team')
            ->join('bl_player_type', 'bl_players.player_type', '=', 'bl_player_type.id')
            ->select('bl_team.id as team_id','bl_team.team_name','bl_team.team_logo','bl_players.id as player_id','bl_players.player_name','bl_players.player_photo','bl_players.player_type','bl_player_type.player_type')
            ->get();   


        if(!empty($all_data)){
            $arrr = array();
            foreach($all_data as $obj){                
                $arrr[$obj->team_id][] = array(
                    'team_name' => $obj->team_name,
                    'team_logo' => $obj->team_logo,
                    'player_id' => $obj->player_id,
                    'player_name' => $obj->player_name,
                    'player_photo' => $obj->player_photo,
                    'player_photo' => $obj->player_photo
                );
            }
        }   
 
        return View::make('front.player')->with('all_data', $arrr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cplPlayersDetail($id)
    {

          $player_data = DB::table('bl_players')
            ->join('bl_player_type', 'bl_players.player_type', '=', 'bl_player_type.id')
            ->join('bl_team', 'bl_players.team', '=', 'bl_team.id')
            ->select('bl_players.id','bl_players.player_name','bl_players.player_photo', 'bl_team.id as team_id' , 'bl_team.team_name','bl_player_type.player_type')
            ->where('bl_players.id', $id)
            ->get();   
            //print_r($player_data);die;

        return View::make('front.single_player')->with('player_data', $player_data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cplschedule()
    {
       $match_data = DB::table('bl_matches')
            ->join('bl_team as t1', 'bl_matches.first_team', '=', 't1.id')
            ->join('bl_team as t2', 'bl_matches.second_team', '=', 't2.id')
            ->leftjoin('bl_team as t3', 'bl_matches.result', '=', 't3.id')
            ->leftjoin('bl_umpires as u1', 'bl_matches.first_umpire', '=','u1.id')
            ->leftjoin('bl_umpires as u2', 'bl_matches.second_umpire', '=','u2.id')
            ->select('*','bl_matches.id as match_id','u1.ump_name as first_umpire_name','u2.ump_name as second_umpire_name', 't1.team_name as t1_team_name', 't2.team_name as t2_team_name' ,'t3.team_name as t3_team_name' )
            ->get();
        
        return View::make('front.cplschedule')->with('match_data', $match_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cplScorecard($match_id)
    {
        
        $score_data = DB::table('bl_records')
            ->join('bl_team as t1', 'bl_records.team_id', '=', 't1.id')            
            ->leftjoin('bl_players as p1', 'bl_records.player_id', '=', 'p1.id')
            ->select('bl_records.*','t1.team_name as t1_team_name' ,'p1.player_name as p1_player_name')
            ->where('bl_records.match_id', '=',$match_id)
            ->orderBy('t1.id', 'asc')
            ->orderBy('bl_records.down', 'asc')
            ->get();            

        return View::make('front.scorecard')->with('score_data', $score_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function cplTopplayers(){        
        $top_bastman = DB::table('bl_records')
            ->select(DB::raw('sum(bl_records.run) as total_run, sum(bl_records.ball) as total_ball, bl_records.player_id, bl_players.player_name, bl_team.team_logo, bl_team.team_name'))
            ->join('bl_players', 'bl_players.id', '=', 'bl_records.player_id')
            ->join('bl_team', 'bl_team.id', '=', 'bl_records.team_id')    
            ->groupBy('player_id')
            ->orderBy('total_run', 'desc')
            ->orderBy('total_ball', 'asc')               
            ->get();        
        $top_bowller = DB::table('bl_records')
            ->select(DB::raw('sum(bl_records.wicket) as total_wicket, sum(bl_records.given_run) as total_given_run, sum(bl_records.over) as total_over, bl_players.player_name, bl_team.team_logo, bl_team.team_name'))
            ->join('bl_players', 'bl_players.id', '=', 'bl_records.player_id')
            ->join('bl_team', 'bl_team.id', '=', 'bl_records.team_id')             
            ->groupBy('player_id')
            ->orderBy('total_wicket', 'desc')
            ->orderBy('total_given_run', 'asc')               
            ->get();
        $matchesdata = DB::table('bl_matches')            
            ->join('bl_team as t1', 'bl_matches.first_team', '=', 't1.id')
            ->join('bl_team as t2', 'bl_matches.second_team', '=', 't2.id')
            ->select('bl_matches.id as match_id','bl_matches.match_date','t1.id as t1_id' , 't2.id as t2_id' , 't1.team_logo as t1_team_logo','t2.team_logo as t2_team_logo','t1.team_name as t1_team_name','t2.team_name as t2_team_name', 'bl_matches.first_team_run','bl_matches.first_team_over', 'bl_matches.second_team_run','bl_matches.second_team_over','bl_matches.first_team','bl_matches.second_team', 'bl_matches.result')
            ->get();         
        $top_teams = array();
        if(!empty($matchesdata)){
            foreach($matchesdata as $objMatch){                 
                if(!empty($objMatch->first_team_run)){
                    
//                    $per = ($first_over_arr[1] / 6 * 100);
//                    $colper = (10 * $per)/100;
//                    $removedot = explode(".", $colper);
//                    $first_team_over_after_call = $first_over_arr[0] . "." . $removedot[0];
//                    
//                    $second_over_arr = explode('.', $objMatch->second_team_over);
//                    $per1 = ($second_over_arr[1] / 6 * 100);
//                    $colper1 = (10 * $per1)/100;
//                    $removedot1 = explode(".", $colper1);
//                    $second_team_over_after_call = $second_over_arr[0] . "." . $removedot1[0];
                    
                    $first_over_arr = explode('.', $objMatch->first_team_over);//                    
                    $first_team_over_after_call = $first_over_arr[0]  + ($first_over_arr[1] / 6);
                    
                    $second_over_arr = explode('.', $objMatch->second_team_over);
                    $second_team_over_after_call = $second_over_arr[0] + ($second_over_arr[1] / 6) ;
                    
                    
                    // $first_team_avg = ($objMatch->first_team_run/$first_team_over_after_call) - ($objMatch->second_team_run/$second_team_over_after_call);
                    // $second_team_avg = ($objMatch->second_team_run/$second_team_over_after_call) - ($objMatch->first_team_run/$first_team_over_after_call);
                    $top_teams[$objMatch->first_team]['first_team']['run'][] = $objMatch->first_team_run;
                    $top_teams[$objMatch->first_team]['first_team']['over'][] = $first_team_over_after_call;
                    $top_teams[$objMatch->first_team]['second_team']['run'][] = $objMatch->second_team_run;
                    $top_teams[$objMatch->first_team]['second_team']['over'][] = $second_team_over_after_call;
                    $top_teams[$objMatch->second_team]['first_team']['run'][] = $objMatch->second_team_run;
                    $top_teams[$objMatch->second_team]['first_team']['over'][] = $second_team_over_after_call;
                    $top_teams[$objMatch->second_team]['second_team']['run'][] = $objMatch->first_team_run;
                    $top_teams[$objMatch->second_team]['second_team']['over'][] = $first_team_over_after_call;
                }
              
            }            
        }
        $top_team_array = array();
       if(!empty($top_teams)){
            foreach($top_teams as $key=>$objteams){                
                $length = count($objteams['first_team']['run']);
                $i = 0;
                $total_run_first = 0;
                $total_over_first = 0;
                $total_run_second = 0;
                $total_over_second = 0;
                while($length > $i){
                    $total_run_first = $total_run_first + $objteams['first_team']['run'][$i];
                    $total_over_first = $total_over_first + $objteams['first_team']['over'][$i];
                    $total_run_second = $total_run_second + $objteams['second_team']['run'][$i];
                    $total_over_second = $total_over_second + $objteams['second_team']['over'][$i];
                    $i++;
                } 
                $total_first =  $total_run_first / $total_over_first ;           
                $total_second =  $total_run_second / $total_over_second ; 
                $total =   $total_first - $total_second;
                $top_team_array[$key] = round($total, 3); 
            }
        }
        $finalMatchPointArr = array();
        if(!empty($top_team_array)){
            foreach($top_team_array as $key=>$finalObj){                
                $totalwin = DB::table('bl_matches') 
                ->join('bl_team', 'bl_team.id', '=', 'bl_matches.result')
                ->select('bl_team.team_name', 'bl_team.team_logo')
                ->where('bl_matches.result', '=', $key)        
                ->get();                
                $point = (count($totalwin) * 2);
                if(empty($totalwin)){
                   $totalwin = DB::table('bl_team')                     
                    ->select('bl_team.team_name', 'bl_team.team_logo')
                    ->where('bl_team.id', '=', $key)        
                    ->get();  
                }
                $finalMatchPointArr[$key]['point'] = $point;
                $finalMatchPointArr[$key]['avg'] = $finalObj;
                $finalMatchPointArr[$key]['team_name'] = isset($totalwin[0]->team_name)?$totalwin[0]->team_name:"";
                $finalMatchPointArr[$key]['team_logo'] = isset($totalwin[0]->team_logo)?$totalwin[0]->team_logo:"";
            }            
        }
        $column1 = array();
        $column2 = array();
        $column3 = array();        
        if(!empty($finalMatchPointArr)){
            foreach($finalMatchPointArr as $key => $obj){
                    $column1[$key] = $obj['point'];
                    $column2[$key] = $obj['avg'];
                    $column3[$key] = $obj['team_name'];
            }
        }
        array_multisort($column1, SORT_DESC, $column2, SORT_DESC, $finalMatchPointArr);
        return View::make('front.topplayers')->with(array('top_batsman'=>$top_bastman, 'top_bowller'=>$top_bowller, 'top_teams'=>$finalMatchPointArr));
    }
}

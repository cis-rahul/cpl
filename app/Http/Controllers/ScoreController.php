<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\RequestFirstTeamData;
use App\Http\Controllers\Controller;
use App\Matches;
use App\Umpires;
use DB;
use View;
use Redirect;
use App\Records;

class ScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ftm_id,$stm_id,$match_id)
    {        
        $ftm_player = array();
        $stm_player = array();
        //get first team player
        $f_players = DB::select('select id, player_name from bl_players where team = :team', ['team' => $ftm_id]);
        $umpires = Umpires::select('id','ump_name')->where('status','=','1')->orderBy('id','desc')->get()->toArray();  
        $umpire_name[''] = "Select Team";
        foreach($umpires as $data) {
            $umpire_name[$data['id']] = $data['ump_name'];
        }        
        foreach($f_players as $fp){
            $ftm_player[$fp->id] = $fp->player_name;
        }
        //$player_data = explode('_', $ftm_player);
        //get second team player
        $s_players = DB::select('select player_name,id from bl_players where team = :team', ['team' => $stm_id]);
        $matchinfo = Matches::find($match_id)->toArray();

        foreach($s_players as $sp){
            $stm_player[$sp->id] = $sp->player_name;
        }
        $finalArray[$match_id][$ftm_id] = $ftm_player;
        $finalArray[$match_id][$stm_id] = $stm_player;
        //to get team name 
        $ftm  = DB::select('select team_name from bl_team where id = :id', ['id' => $ftm_id]);
        $first_team = $ftm[0]->team_name;
        
        $stm  = DB::select('select team_name from bl_team where id = :id', ['id' => $stm_id]);
        $second_team = $stm[0]->team_name; 
        
        $str = Records::select('*')->where('match_id', '=', $match_id)->get()->toArray();
        $records = array();
        if(!empty($str)){            
            foreach($str as $obj){
                $records[$obj['team_id']][$obj['player_id']]['id'] = $obj['id'];
                $records[$obj['team_id']][$obj['player_id']]['run'] = $obj['run'];
                $records[$obj['team_id']][$obj['player_id']]['ball'] = $obj['ball'];
                $records[$obj['team_id']][$obj['player_id']]['four'] = $obj['four'];
                $records[$obj['team_id']][$obj['player_id']]['six'] = $obj['six'];
                $records[$obj['team_id']][$obj['player_id']]['over'] = $obj['over'];
                $records[$obj['team_id']][$obj['player_id']]['noball'] = $obj['noball'];
                $records[$obj['team_id']][$obj['player_id']]['wideball'] = $obj['wideball'];
                $records[$obj['team_id']][$obj['player_id']]['madian'] = $obj['madian'];
                $records[$obj['team_id']][$obj['player_id']]['wicket'] = $obj['wicket'];
                $records[$obj['team_id']][$obj['player_id']]['given_run'] = $obj['given_run'];
                $records[$obj['team_id']][$obj['player_id']]['created_at'] = $obj['created_at'];
                $records[$obj['team_id']][$obj['player_id']]['updated_at'] = $obj['updated_at'];
                $records[$obj['team_id']][$obj['player_id']]['status'] = $obj['status'];
                $records[$obj['team_id']][$obj['player_id']]['under_eleven'] = $obj['under_eleven'];
                $records[$obj['team_id']][$obj['player_id']]['down'] = $obj['down'];
                $records[$obj['team_id']][$obj['player_id']]['out_not'] = $obj['out_not'];
            }           
        }        
        return View::make('admin.enterscore', array('final' => $finalArray ,'first_team_name' => $first_team , 'second_team_name' => $second_team,'first_team_id' => $ftm_id, 'second_team_id' => $stm_id, 'first_team_id'=>$ftm_id, 'second_team_id'=>$stm_id,'match_id'=>$match_id, 'records'=>$records, 'umpires'=>$umpire_name, 'matchinfo'=>$matchinfo));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function AddRecords(RequestFirstTeamData $request)
    {   
        $first_team_id = $request->input('first_team_id');
        $second_team_id = $request->input('second_team_id');
        $match_id = $request->input('match_id');        
        $run = $request->input('runs');
        $ball = $request->input('ball');
        $fours = $request->input('fours');
        $six = $request->input('six');
        $overs = $request->input('overs');
        $no_ball = $request->input('no_ball');
        $wide = $request->input('wide');
        $maiden = $request->input('maiden');
        $wicket = $request->input('wicket');
        $runs_given = $request->input('runs_given');
        $team_id = $request->input('team_id');
        $down = $request->input('down');
        $out_not = $request->input('out_not');
        $under_eleven = $request->input('under_eleven');


        $ids = $request->input('ids');
        
        if(!empty($run)){                
          foreach($run as $key => $objrun){
              foreach($objrun as $objKey => $objrunfirst){               
                if(in_array($objKey, $under_eleven)){
                    $id = ($ids[$key][$objKey] == "")?"":$ids[$key][$objKey];
                    if($id == ""){
                        $model = new Records();
                        $model->created_at = date("Y-m-d H:i:s");
                        $model->updated_at = date("Y-m-d H:i:s");
                        $model->status = '1';
                    }else{
                        $model = Records::find($id);
                    }
                    //$model = new Records();                
                    $model->match_id = $key;
                    $model->player_id = $objKey;
                    $model->team_id = $team_id;                
                    $model->run = ($objrunfirst == "")?0:$objrunfirst;                                
                    $model->ball = ($ball[$key][$objKey] == "")?0:$ball[$key][$objKey];                
                    $model->four = ($fours[$key][$objKey] == "")?0:$fours[$key][$objKey];                
                    $model->six = ($six[$key][$objKey] == "")?0:$six[$key][$objKey];
                    $model->over = ($overs[$key][$objKey] == "")?0:$overs[$key][$objKey];                
                    $model->noball = ($no_ball[$key][$objKey] == "")?0:$no_ball[$key][$objKey];
                    $model->wideball = ($wide[$key][$objKey] == "")?0:$wide[$key][$objKey];                
                    $model->madian = ($maiden[$key][$objKey] == "")?0:$maiden[$key][$objKey];
                    $model->wicket = ($wicket[$key][$objKey] == "")?0:$wicket[$key][$objKey];
                    $model->given_run = ($runs_given[$key][$objKey] == "")?0:$runs_given[$key][$objKey];

                    $model->down = ($down[$key][$objKey] == "")?0:$down[$key][$objKey];
                    $model->out_not = (isset($out_not[$key][$objKey]))?'1':'0';               
                    $model->under_eleven = '1';               
                    $model->save();
                }                
              }               
          }
          $first_team_id = trim($first_team_id);
          $second_team_id = trim($second_team_id);
          $match_id = trim($match_id);
          return Redirect::to(route('admin.addscore', array('ftm'=>$first_team_id,'stm'=>$second_team_id,'match_id'=>$match_id)));
        }        
        
    }



    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

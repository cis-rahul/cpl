<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\LoginRequestData;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Auth;
use View;
use Response;
use Redirect;
use Session;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index()
    {   

        $email = Session::get('email');
        if(isset($email) && $email){
            return Redirect::to(route('admin.dashbaord'))->with('message', 'Logged in successfully');
        }else{
            return Redirect::to(route('admin.loginpage'))->with('message', 'Records are not matching');
        }
        
    }

    public function loginView(){
        $email = Session::get('email');
        if(isset($email) && $email){
            return Redirect::to(route('admin.dashbaord'))->with('message', 'Logged in successfully');
        }else{
            return View::make('admin.layouts.login')->with('message', 'Records are not matching');
        }

    }
 
    public function auth(LoginRequestData $request)
    {   
         // $email = Session::get('email');
         // if(isset($email) && $email) {
         //     return Redirect::to(route('admin.dashbaord'));
         // }else{

           $email = $request->get('email');
           $password = $request->get('password');

           $email_value = 'cishradmin@cisin.in';
           $pass_val = 'admin123';

        if($email_value == $email &&  $pass_val = $password){
                
                Session::put('email', $email);
                Session::put('password', $password);

                return Redirect::to(route('admin.dashbaord'))->with('message', 'Logged in successfully');
        }else{
            return Redirect::to(route('admin.loginpage'))->with('message', 'Records are not matching');
        }
        //}

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {

        $email = Session::get('email');
        if(isset($email) && $email){
            Auth::logout();
            Session::flush();
            return Redirect::to(route('admin.loginpage'));
        }else{
            return Redirect::to(route('admin.loginpage'));
        }
    }
}

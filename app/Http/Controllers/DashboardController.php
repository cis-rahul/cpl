<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use Auth;
use View;

class DashboardController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __construct() {
        
    }

    public function index() {
        $email = Session::get('email');
        if(isset($email) && $email){
            return view('admin.child');
        }else{
            return Redirect::to(route('admin.loginpage'))->with('message', 'Fill details to login');
        }
        
    }

}

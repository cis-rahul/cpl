<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Umpires extends Model
{
     protected $table = 'bl_umpires';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $fillable = ['id','ump_name','ump_team_name'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Records extends Model
{
   protected $table = 'bl_records';
   public function __construct(array $attributes = array()) {
       
       parent::__construct($attributes);       
   }
   
   //protected $fillable = ['id','first_team','second_team','match_date'];
}

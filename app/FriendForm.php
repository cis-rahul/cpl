<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FriendForm extends Model
{
     protected $table = 'bl_session';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','session_name'];
    
    
}

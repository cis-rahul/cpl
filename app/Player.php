<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $table = 'bl_players';
    protected $fillable = ['id','player_name','team','player_photo','player_type'];
}

@extends('admin.layouts.master')

@section('content')
  {!! HTML::script('http://code.jquery.com/jquery-1.10.2.js') !!}
  {!! HTML::script('http://code.jquery.com/ui/1.11.4/jquery-ui.js') !!}

<div class="side-body padding-top">
 
    <div id="exTab2" class="container"> 
        <ul class="nav nav-tabs">
            <li class="active" id="one">
                <a  href="#1" data-toggle="tab">{{ $first_team_name }}</a>
            </li>
            <li>
                <a href="#2" data-toggle="tab">{{ $second_team_name }}</a>
            </li>
            <li id="three">
                <a href="#3" data-toggle="tab">Match Result</a>
            </li>
        </ul>

            <div class="tab-content ">
              <div class="tab-pane active" id="1">
                 {!! Form::open(array('route' => 'admin.firstmatchcontent', 'class' => 'form' , 'id' => 'one_form' , 'method' => 'post')) !!}
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Player Name</th>
                                <th>R</th>
                                <th>B</th>
                                <th>4s</th>
                                <th>6s</th>
                                <th>SR</th>
                                <th>Over</th>
                                <th>No Ball</th>
                                <th>Wide</th>
                                <th>Maiden</th>
                                <th>Wicket</th>
                                <th>Runs Given</th>
                              </tr>

                            </thead>
                            <tbody>
                              
                              @foreach($first_team_players as $data) 
                                
                            <tr>
                                <th>{!! explode('_', $data)  !!}</th>
                                <th> <div class="form-group">
                                            {!! Form::text('runs', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Runs')) !!}
                                     </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('ball', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Balls')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('fours', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Fours')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('six', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Six')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('strik_rate', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Strik Rate')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('overs', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Overs')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('no_ball', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'No Ball')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('wide', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Wide')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('maiden', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Maiden')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('wicket', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Wicket')) !!}
                                    </div>
                                </th>
                                 <th>
                                    <div class="form-group">
                                            {!! Form::text('runs_given', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Given Runs')) !!}
                                    </div>
                                </th>
                            </tr>

                            @endforeach

                            </tbody>
                          </table>          
                    

                          <div class="form-group" style="margin-left:50%;">
                            <button type="submit" class="btn btn-default">Submit</button>
                         </div>

                         {!! Form::close() !!}
                
        </div>
               


            <div class="tab-pane" id="2">
                    {!! Form::open(array('route' => 'admin.matchupdate', 'class' => 'form' , 'id' => 'second_form' , 'method' => 'post')) !!}
                        <table class="table">
                            <thead>
                              <tr>
                                <th>Player Name</th>
                                <th>R</th>
                                <th>B</th>
                                <th>4s</th>
                                <th>6s</th>
                                <th>SR</th>
                                <th>Over</th>
                                <th>No Ball</th>
                                <th>Wide</th>
                                <th>Maiden</th>
                                <th>Wicket</th>
                                <th>Runs Given</th>
                              </tr>

                            </thead>
                            <tbody>
                                
                              @foreach($second_team_players as $data) 

                            <tr>
                                <th>{{ $data }}</th>
                                <th> <div class="form-group">
                                            {!! Form::text('runs', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Runs')) !!}
                                     </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('ball', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Balls')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('fours', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Fours')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('six', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Six')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('strik_rate', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Strik Rate')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('overs', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Overs')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('no_ball', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'No Ball')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('wide', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Wide')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('maiden', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Maiden')) !!}
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                            {!! Form::text('wicket', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Wicket')) !!}
                                    </div>
                                </th>
                                 <th>
                                    <div class="form-group">
                                            {!! Form::text('runs_given', null, 
                                            array(
                                            'class'=>'form-control', 
                                            'placeholder'=>'Given Runs')) !!}
                                    </div>
                                </th>
                            </tr>

                            @endforeach

                            </tbody>
                          </table>  

                          <div class="form-group" style="margin-left:50%;">
                            <button type="submit" class="btn btn-default">Submit</button>
                         </div>
                         {!! Form::close() !!}
            </div>


            <div class="tab-pane active" id="3">
            {!! Form::open(array('route' => 'admin.matchupdate', 'class' => 'form' , 'id' => 'three_form' , 'method' => 'post')) !!}
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Winner Team</th>
                              </tr>

                            </thead>
                            <tbody>
                                <tr>
                                    <th>
                                         <div class="form-group">
                                            <p>Smartians Tuskers</p>
                                        </div>
                                    </th>
                            </tr>
                            </tbody>
                          </table>  
                       <div class="form-group" style="margin-left:50%;">
                            <button type="submit" class="btn btn-default">Submit</button>
                         </div>
                 {!! Form::close() !!}           
                
        </div>
    </div>
            
  </div> 
              
 
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#3').hide();

        $('#three').click(function(){
            $('#3').show();
        });
        $('#one').click(function(){
            $('#3').hide();
        });

    });
     
</script>
@endsection

@extends('admin.layouts.master')

@section('content')
 
<div class="side-body padding-top">
    <ul>
        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    
    <h4>Update Player </h4>


    {!! Form::open(array('route' => 'admin.updateump', 'class' => 'form' , 'id' => 'save_ump' , 'method' => 'post')) !!}
    
    <div class="form-group">
        {!! Form::hidden('ump_id', $selected_data['id']) !!}
    </div>

    <div class="form-group">

        {!! Form::label('Umpire Name') !!}
        {!! Form::text('umpire_name', $selected_data['ump_name'], 
        array(
        'class'=>'form-control', 
        'placeholder'=>'Umpire name')) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('Team', 'Team Name') !!}
        {!! Form::select('umpire_team_name', $team_data,$selected_data['ump_team_name'], array("class"=>"form-control") ) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Add', 
        array('class'=>'btn btn-primary')) !!}
    </div>
    {!! Form::close() !!}
</div>


<style type="text/css">
     .logo{border:1px solid #CCCCCC ; border-radius:5px; }
</style>


@endsection



@extends('admin.layouts.master')

@section('content')
    
<div class="side-body padding-top">
    <ul>
        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    
    <h4>Add Team   <a href="{{ route("admin.allteams") }}">
        <span style="color:green; float:right; font-size:15px;" class="title">List Teams</span>
    </a></h4>


    {!! Form::open(array('route' => 'admin.teamadd', 'class' => 'form' , 'id' => 'save_team' , 'method' => 'post' , 'files' => 'true')) !!}

    <div class="form-group">
        {!! Form::label('Team Name') !!}
        {!! Form::text('team_name', null, 
        array(
        'class'=>'form-control', 
        'placeholder'=>'Team name')) !!}
    </div>


    <div class="form-group">
        {!! Form::label('Manager') !!}
        {!! Form::text('team_manager', null, 
        array(
        'class'=>'form-control', 
        'placeholder'=>'Team manager')) !!}
    </div>
 

    <div class="form-group">
        {!! Form::label('Coach') !!}
        {!! Form::text('team_coach', null, 
        array(
        'class'=>'form-control', 
        'placeholder'=>'Team coach')) !!}
    </div>

      <div class="form-group">
        {!! Form::label('Logo') !!}
        {!! Form::file('team_logo', null, 
        array(
        'class'=>'form-control',)) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Add', 
        array('class'=>'btn btn-primary')) !!}
    </div>
    {!! Form::close() !!}
</div>
<style>
    .container-fluid-session{margin: 7% 0 0 13%; position: fixed; width:72%;}
</style>
@endsection

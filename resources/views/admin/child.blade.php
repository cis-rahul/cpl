@extends('admin.layouts.master')

@section('content')

<div class="container-fluid">
    <div class="side-body padding-top">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="#">
                    <div class="card red summary-inline">
                        <div class="card-body">
                            <i class="icon fa fa-cubes fa-4x"></i>
                            <div class="content">
                                <div class="title">15</div>
                                <div class="sub-title">Teams</div>
                            </div>
                            <div class="clear-both"></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="#">
                    <div class="card yellow summary-inline">
                        <div class="card-body">
                            <i class="icon fa fa-user fa-4x"></i>
                            <div class="content">
                                <div class="title">230</div>
                                <div class="sub-title">Players</div>
                            </div>
                            <div class="clear-both"></div>
                        </div>
                    </div>
                </a>
            </div>
            
        </div>
    </div>
</div> 
@endsection

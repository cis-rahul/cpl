@extends('admin.layouts.master')

@section('content')
 
<div class="side-body padding-top">
    <ul>
        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    
    <h4>Add Player   <a href="{{ route("admin.allump") }}">
        <span style="color:green; float:right; font-size:15px;" class="title">List Umpires</span>
    </a></h4>


    {!! Form::open(array('route' => 'admin.umpadd', 'class' => 'form' , 'id' => 'save_ump' , 'method' => 'post')) !!}

    <div class="form-group">

        {!! Form::label('Umpire Name') !!}
        {!! Form::text('umpire_name', null, 
        array(
        'class'=>'form-control', 
        'placeholder'=>'Umpire name')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('Team', 'Team Name') !!}
        {!! Form::select('umpire_team_name', $team_data,'null', array("class"=>"form-control") ) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Add', 
        array('class'=>'btn btn-primary')) !!}
    </div>
    {!! Form::close() !!}
</div>


@endsection



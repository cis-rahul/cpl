@extends('admin.layouts.master')

@section('content')

<div class="side-body padding-top">
    <ul>
        @if(Session::has('message'))
                <p style="color: green; font-size: 15px;" class="">{{ Session::get('message') }}</p>
                @endif

        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">

                    <div class="card-title" style="width:100%">
                        <div class="title">List of Umpire's    
                              <a href="{{ route("admin.addump") }}">
                                    <span style="color:green; float:right; font-size:15px;" class="title">Add Umpire</span>
                              </a>
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <table class="datatable table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Umpaire Name</th>
                                <th>Team Name</th>
                                <th>Created Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>

                             @foreach($ump_data as $data) 

                            <tr>
                                <th>{{$data->id}}</th>
                                <th>{{$data->ump_name}}</th>
                                <th>{{$data->team_name}}</th>
                                <th>{{$data->created_at}}</th>
                                <th>
                                    <a alt="update" href="{{ route('admin.editump', $data->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;&nbsp;&nbsp;
                                    <a alt="delete" href="{{route('admin.deleteump', $data->id)}}"> <span class="glyphicon glyphicon-remove"></span></a>
                                </th>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

@endsection

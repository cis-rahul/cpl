@extends('admin.layouts.master')

@section('content')
 
<div class="side-body padding-top">
    <ul>
        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    
    <h4>Add Player   <a href="{{ route("admin.allplayers") }}">
        <span style="color:green; float:right; font-size:15px;" class="title">List Players</span>
    </a></h4>


    {!! Form::open(array('route' => 'admin.playeradd', 'class' => 'form' , 'id' => 'save_player' , 'method' => 'post' , 'files' => 'true')) !!}

    <div class="form-group">

        {!! Form::label('Player Name') !!}
        {!! Form::text('player_name', null, 
        array(
        'class'=>'form-control', 
        'placeholder'=>'Player name')) !!}
    </div>


   <div class="form-group">
        {!! Form::label('Team Name') !!}<br />
        {!! Form::select('team_name', $team_data,'null', array("class"=>"form-control") ) !!}
    </div>


    <div class="form-group">
        {!! Form::label('item', 'Player Type') !!}
        {!! Form::select('player_type', $player_type,'null', array("class"=>"form-control") ) !!}
    </div>


      <div class="form-group">
        {!! Form::label('Player DP') !!}
        {!! Form::file('player_photo', null, 
        array(
        'class'=>'form-control',)) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Add', 
        array('class'=>'btn btn-primary')) !!}
    </div>
    {!! Form::close() !!}
</div>
@endsection
<!DOCTYPE html>
<html>
    @include('admin.common.head')
    <body class="flat-blue">
        <div class="app-container">
            <div class="row content-container">
                @include('admin.common.header')
                @include('admin.common.leftsidebar')

                @yield('content')

               
            </div>
            @include('admin.common.footer')
        </div>   
    </body>
</html>

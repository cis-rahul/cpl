@extends('admin.layouts.master')

@section('content')

<div class="side-body padding-top">
    <ul>
        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    
    <h4>Update Team</h4>


    {!! Form::open(array('route' => 'admin.updateam', 'class' => 'form' , 'id' => 'save_session' , 'method' => 'post', 'files' => 'true')) !!}

    <div class="form-group">
        {!! Form::label('Team Name') !!}
        {!! Form::text('team_name', $selected_team->team_name, 
        array(
        'class'=>'form-control', 
        'placeholder'=>'Team name')) !!}
    </div>

    <div class="form-group">
        {!! Form::hidden('team_id', $selected_team->id) !!}
    </div>


    <div class="form-group">
        {!! Form::label('Manager') !!}
        {!! Form::text('team_manager',  $selected_team->team_manager, 
        array(
        'class'=>'form-control', 
        'placeholder'=>'Team manager')) !!}
    </div>
 

    <div class="form-group">
        {!! Form::label('Coach') !!}
        {!! Form::text('team_coach', $selected_team->team_coach, 
        array(
        'class'=>'form-control', 
        'placeholder'=>'Team coach')) !!}
    </div>

      <div class="form-group">
        {!! Form::label('Logo') !!}
        {!! Form::hidden('team_logo', $selected_team->team_logo, 
        array(
        'class'=>'form-control',)) !!}
        
        {!! HTML::image('uploads/'.$selected_team->team_logo, 'logo', array( 'class' => 'logo' , 'width' => 100, 'height' => 80 )) !!}
      </div>

       <div class="form-group">
        {!! Form::file('team_logo', null, 
        array(
        'class'=>'form-control',)) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Update', 
        array('class'=>'btn btn-primary')) !!}
    </div>
    {!! Form::close() !!}
</div>
<style>
    .logo{border:1px solid #CCCCCC ; border-radius:5px; }
</style>
@endsection
<head>
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    {!! HTML::style('http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400') !!}
    {!! HTML::style('http://fonts.googleapis.com/css?family=Lato:300,400,700,900') !!}

    <!-- CSS Libs -->
    {!! HTML::style('admin-theme/bower_components/bootstrap/dist/css/bootstrap.css') !!}
    {!! HTML::style('admin-theme/bower_components/fontawesome/css/font-awesome.min.css') !!}
    {!! HTML::style('admin-theme/bower_components/animate.css') !!}
    {!! HTML::style('admin-theme/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') !!}
    {!! HTML::style('admin-theme/bower_components/iCheck/skins/flat/_all.css') !!}
    {!! HTML::style('admin-theme/bower_components/DataTables/media/css/jquery.dataTables.min.css') !!}
    {!! HTML::style('admin-theme/bower_components/select2/dist/css/select2.min.css') !!}
    {!! HTML::style('admin-theme/vendor/css/dataTables.bootstrap.css') !!}
    {!! HTML::style('admin-theme/css/style.css') !!}
    {!! HTML::style('http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css') !!}
    <!-- CSS App -->
    {!! HTML::style('admin-theme/css/style.css') !!}
    {!! HTML::style('admin-theme/css/themes.css') !!}

<!-- Js App -->
    {!! HTML::script('admin-theme/bower_components/jquery/dist/jquery.min.js') !!}
    {!! HTML::script('admin-theme/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
    
</head>
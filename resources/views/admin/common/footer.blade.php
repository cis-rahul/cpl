 
<footer class="app-footer">
    <div class="wrapper">
        <span class="pull-right">2.0 <a href=""><i class="fa fa-long-arrow-up"></i></a></span> © 2015 Copyright.
    </div>
</footer>
 
    <!-- Javascript Libs -->
    {!! HTML::script('admin-theme/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js') !!}
    {!! HTML::script('admin-theme/bower_components/iCheck/icheck.min.js') !!}
    {!! HTML::script('admin-theme/bower_components/matchHeight/jquery.matchHeight-min.js') !!}
    {!! HTML::script('admin-theme/bower_components/DataTables/media/js/jquery.dataTables.min.js') !!}
    {!! HTML::script('admin-theme/bower_components/select2/dist/js/select2.full.min.js') !!}
   
    
    <!-- Javascript -->
    {!! HTML::script('admin-theme/js/app.js') !!}

@extends('admin.layouts.master')

@section('content')
    
<div class="side-body padding-top">
    <ul>
        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    
    <h4>Add Season   <a href="{{ route("admin.allsession") }}">
        <span style="color:green; float:right; font-size:15px;" class="title">List Season</span>
    </a></h4>


    {!! Form::open(array('route' => 'admin.addsession', 'class' => 'form' , 'id' => 'save_session' , 'method' => 'post')) !!}

    <div class="form-group">
        {!! Form::label('Season') !!}
        {!! Form::text('session_name', null, 
        array(
        'class'=>'form-control', 
        'placeholder'=>'Add Season')) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Add', 
        array('class'=>'btn btn-primary')) !!}
    </div>
    {!! Form::close() !!}
</div>
 
@endsection

@extends('admin.layouts.master')

@section('content')
  {!! HTML::script('http://code.jquery.com/jquery-1.10.2.js') !!}
  {!! HTML::script('http://code.jquery.com/ui/1.11.4/jquery-ui.js') !!}
  
<div class="side-body padding-top">
    <ul>
        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    
    <h4><i>Upate Schedule Match</i></h4>

    {!! Form::open(array('route' => 'admin.matchupdate', 'class' => 'form' , 'id' => 'update_matach' , 'method' => 'post')) !!}
 
   <div class="form-group">
        {!! Form::label('Select First Team') !!}<br />
        {!! Form::select('team_name_first', $team_data,$match_data->first_team, array("class"=>"form-control") ) !!}
    </div>

    <div class="form-group">
        {!! Form::hidden('match_id', $match_data->id) !!}
    </div>


  <div class="form-group">
        {!! Form::label('Select Second Team') !!}<br />
        {!! Form::select('team_name_second', $team_data,$match_data->second_team, array("class"=>"form-control") ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('Pick a Date') !!}<br />
        {!! Form::text('datepicker',$match_data->match_date, array('class'=>'form-control','placeholder'=>'Pick date please' , 'id'=>'datepicker')) !!}
    </div>
 
    <div class="form-group">
        {!! Form::submit('Update', 
        array('class'=>'btn btn-primary')) !!}
    </div>
    {!! Form::close() !!}
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $( "#datepicker" ).datepicker();     
    });
     
</script>

@endsection



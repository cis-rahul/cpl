@extends('admin.layouts.master')

@section('content')
 
<div class="side-body padding-top">
    <ul>
        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    
    <h4>Update Player </h4>


    {!! Form::open(array('route' => 'admin.updateplayer', 'class' => 'form' , 'id' => 'update_player' , 'method' => 'post' , 'files' => 'true')) !!}

    <div class="form-group">

        {!! Form::label('Player Name') !!}
        {!! Form::text('player_name', $selected_player['player_name'], 
        array(
        'class'=>'form-control', 
        'placeholder'=>'Player name')) !!}
    </div>

    <div class="form-group">
        {!! Form::hidden('player_id', $selected_player['id']) !!}
    </div>

   <div class="form-group">
        {!! Form::label('Team Name') !!}
        {!! Form::select('team_name', $team_data, $selected_player['team'] , array("class"=>"form-control") ) !!}
    </div>


    <div class="form-group">
        {!! Form::label('item', 'Player Type') !!}
        {!! Form::select('player_type', $player_type, $selected_player['player_type']  , array("class"=>"form-control") ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('Player DP') !!}
        {!! Form::hidden('player_photo', $selected_player['player_photo'], 
        array(
        'class'=>'form-control',)) !!}
        
        {!! HTML::image('uploads/'. $selected_player['player_photo'] , 'Player DP', array( 'class' => 'logo' , 'width' => 100, 'height' => 80 )) !!}
      </div>

       <div class="form-group">
        {!! Form::file('player_photo', null, 
        array(
        'class'=>'form-control',)) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Upadate', 
        array('class'=>'btn btn-primary')) !!}
    </div>
    {!! Form::close() !!}
</div>
<style type="text/css">
     .logo{border:1px solid #CCCCCC ; border-radius:5px; }
</style>


@endsection



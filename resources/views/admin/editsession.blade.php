@extends('admin.layouts.master')

@section('content')

<div class="side-body padding-top">
    <ul>
        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    
    <h4>Update Season</h4>


    {!! Form::open(array('route' => 'admin.updatesession', 'class' => 'form' , 'id' => 'save_session' , 'method' => 'post')) !!}

    <div class="form-group">
        {!! Form::label('Season') !!}
        {!! Form::text('session_name', $selected_session->session_name, 
        array(
        'class'=>'form-control', 
        'placeholder'=>'Season',)) !!}
    </div>
    <div class="form-group">
        {!! Form::hidden('session_id', $selected_session->id) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Update', 
        array('class'=>'btn btn-primary')) !!}
    </div>
    {!! Form::close() !!}
</div>
@endsection

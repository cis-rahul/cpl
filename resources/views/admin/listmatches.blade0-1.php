@extends('admin.layouts.master')

@section('content')

<div class="side-body padding-top">
    <ul>
        @if(Session::has('message'))
                <p style="color: green; font-size: 15px;" class="">{{ Session::get('message') }}</p>
                @endif

        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">

                    <div class="card-title" style="width:100%">
                        <div class="title">List of Matche's    
                              <a href="{{ route("admin.addmatch") }}">
                                    <span style="color:green; float:right; font-size:15px;" class="title">Add New Match</span>
                              </a>
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <table class="datatable table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Team1 </th>
                                <th>V/S</th>
                                <th>Team2</th>
                                <th>Match Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>

                             @foreach($match_data as $data) 

                            <tr>
                                <th>{{$data->id}}</th>
                                <th>{{$data->first_team}}</th>
                                <th>V/s</th>
                                <th>{{$data->second_team}}</th>
                                <th>{{$data->match_date}}</th>
                                <th>
                                    <a alt="update" href="{{ route('admin.editmatch', $data->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;&nbsp;&nbsp;
                                    <a alt="delete" href="{{route('admin.deletematch', $data->id)}}"> <span class="glyphicon glyphicon-remove"></span></a>
                                    <a alt="Put Score" href="{{route('admin.addscore', array('ftm'=>$data->ft_id,'stm'=>$data->fs_id))}}">&nbsp;&nbsp;&nbsp; ENTER RESULT </a>
                                </th>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

@endsection

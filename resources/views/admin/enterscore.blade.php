@extends('admin.layouts.master')

@section('content')
{!! HTML::script('http://code.jquery.com/jquery-1.10.2.js') !!}
{!! HTML::script('http://code.jquery.com/ui/1.11.4/jquery-ui.js') !!}

<div class="side-body padding-top">

    <div id="exTab2" class="container"> 
        <ul class="nav nav-tabs">
            <li class="active" id="one">
                <a  href="#1" data-toggle="tab">{{ $first_team_name }}</a>
            </li>
            <li>
                <a href="#2" data-toggle="tab">{{ $second_team_name }}</a>
            </li>
            <li id="three">
                <a href="#3" data-toggle="tab">Match Result</a>
            </li>
        </ul>        
        <div class="tab-content ">
            <div class="tab-pane active" id="1">
                {!! Form::open(array('route' => 'admin.firstmatchcontent', 'class' => 'form' , 'id' => 'one_form' , 'method' => 'post')) !!}
                <input type='hidden' id='team_first_id' name='team_id' value='{{$first_team_id}}'/>                
                <input type='hidden' name='first_team_id' value='{{$first_team_id}}'/>
                <input type='hidden' name='second_team_id' value='{{$second_team_id}}'/>
                <input type='hidden' name='match_id' value='{{$match_id}}'/>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Player Name</th>
                            <th>R</th>
                            <th>B</th>
                            <th>4s</th>
                            <th>6s</th>
<!--                            <th>SR</th>-->
                            <th>Over</th>
                            <th>No Ball</th>
                            <th>Wide</th>
                            <th>Maiden</th>
                            <th>Wicket</th>
                            <th>Runs Given</th>
                            <th>Down</th>
                            <th>Status</th>
                            <th>Under Eleven</th>
                        </tr>

                    </thead>
                    <tbody>                            
                        @foreach($final as $objKey => $objFinal) 
                        @foreach($objFinal as $objKeyOne => $objFinalOne)

                        @if($first_team_id == $objKeyOne)

                        @foreach($objFinalOne as $objKeySecond => $objFinalSecond) 
                        <?php
                        $run = (isset($records[$objKeyOne][$objKeySecond]['run']) && !empty($records[$objKeyOne][$objKeySecond]['run'])) ? $records[$objKeyOne][$objKeySecond]['run'] : "";
                        $ball = (isset($records[$objKeyOne][$objKeySecond]['ball']) && !empty($records[$objKeyOne][$objKeySecond]['ball'])) ? $records[$objKeyOne][$objKeySecond]['ball'] : "";
                        $fours = (isset($records[$objKeyOne][$objKeySecond]['four']) && !empty($records[$objKeyOne][$objKeySecond]['four'])) ? $records[$objKeyOne][$objKeySecond]['four'] : "";
                        $six = (isset($records[$objKeyOne][$objKeySecond]['six']) && !empty($records[$objKeyOne][$objKeySecond]['six'])) ? $records[$objKeyOne][$objKeySecond]['six'] : "";
                        $overs = (isset($records[$objKeyOne][$objKeySecond]['over']) && !empty($records[$objKeyOne][$objKeySecond]['over'])) ? $records[$objKeyOne][$objKeySecond]['over'] : "";
                        $noball = (isset($records[$objKeyOne][$objKeySecond]['noball']) && !empty($records[$objKeyOne][$objKeySecond]['noball'])) ? $records[$objKeyOne][$objKeySecond]['noball'] : "";
                        $wideball = (isset($records[$objKeyOne][$objKeySecond]['wideball']) && !empty($records[$objKeyOne][$objKeySecond]['wideball'])) ? $records[$objKeyOne][$objKeySecond]['wideball'] : "";
                        $maiden = (isset($records[$objKeyOne][$objKeySecond]['madian']) && !empty($records[$objKeyOne][$objKeySecond]['madian'])) ? $records[$objKeyOne][$objKeySecond]['madian'] : "";
                        $wicket = (isset($records[$objKeyOne][$objKeySecond]['wicket']) && !empty($records[$objKeyOne][$objKeySecond]['wicket'])) ? $records[$objKeyOne][$objKeySecond]['wicket'] : "";
                        $given_run = (isset($records[$objKeyOne][$objKeySecond]['given_run']) && !empty($records[$objKeyOne][$objKeySecond]['given_run'])) ? $records[$objKeyOne][$objKeySecond]['given_run'] : "";
                        $ids = (isset($records[$objKeyOne][$objKeySecond]['id']) && !empty($records[$objKeyOne][$objKeySecond]['id'])) ? $records[$objKeyOne][$objKeySecond]['id'] : "";

                        $down = (isset($records[$objKeyOne][$objKeySecond]['down']) && !empty($records[$objKeyOne][$objKeySecond]['down'])) ? $records[$objKeyOne][$objKeySecond]['down'] : "";
                        $out_not = (isset($records[$objKeyOne][$objKeySecond]['out_not']) && !empty($records[$objKeyOne][$objKeySecond]['out_not'])) ? $records[$objKeyOne][$objKeySecond]['out_not'] : '0';
                        

                        $under_eleven = (isset($records[$objKeyOne][$objKeySecond]['under_eleven']) && !empty($records[$objKeyOne][$objKeySecond]['under_eleven'])) ? $records[$objKeyOne][$objKeySecond]['under_eleven'] : '0';
                        ?>
                        <tr>

                            <th>{!! $objFinalSecond  !!}</th>
                    <input type='hidden' id='ids' name='ids[<?php echo $objKey; ?>][<?php echo $objKeySecond; ?>]' value='<?php echo $ids ?>'/>
                    <th>
                    <div class="form-group">

                        {!! Form::text('runs['.$objKey . ']' .'['. $objKeySecond . ']' , $run , 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Runs')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('ball['.$objKey . ']' .'['. $objKeySecond . ']', $ball, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Balls')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('fours['.$objKey . ']' .'['. $objKeySecond . ']', $fours, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Fours')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('six['.$objKey . ']' .'['. $objKeySecond . ']', $six, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Six')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('overs['.$objKey . ']' .'['. $objKeySecond . ']', $overs, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Overs')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('no_ball['.$objKey . ']' .'['. $objKeySecond . ']', $noball, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'No Ball')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('wide['.$objKey . ']' .'['. $objKeySecond . ']', $wideball, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Wide')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('maiden['.$objKey . ']' .'['. $objKeySecond . ']', $maiden, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Maiden')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('wicket['.$objKey . ']' .'['. $objKeySecond . ']', $wicket, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Wicket')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('runs_given['.$objKey . ']' .'['. $objKeySecond . ']', $given_run, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Given Runs')) !!}
                    </div>
                    </th>

                     <th>
                    <div class="form-group">
                        {!! Form::text('down['.$objKey . ']' .'['. $objKeySecond . ']', $down , 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Down')) !!}
                    </div>
                    </th>

                     <th>
                    <div class="form-group">
                        {!! Form::checkbox('out_not['.$objKey . ']' .'['. $objKeySecond . ']', $objKeySecond , $out_not , ['class' => 'form-control']) !!}                    
                    </div>
                    </th>

                     <th>
                    <div class="form-group">
                        {!! Form::checkbox('under_eleven[]', $objKeySecond , $under_eleven, ['class' => 'form-control']) !!}                    
                    </div>
                    </th>
                    </tr>
                    @endforeach
                    @endif    
                    @endforeach
                    @endforeach

                    </tbody>
                </table>          


                <div class="form-group" style="margin-left:50%;">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>

                {!! Form::close() !!}

            </div>



            <div class="tab-pane" id="2">
                {!! Form::open(array('route' => 'admin.firstmatchcontent', 'class' => 'form' , 'id' => 'second_form' , 'method' => 'post')) !!}
                <input type='hidden' id='team_second_id' name='team_id' value='{{$second_team_id}}'/>
                <input type='hidden' name='first_team_id' value='{{$first_team_id}}'/>
                <input type='hidden' name='second_team_id' value='{{$second_team_id}}'/>
                <input type='hidden' name='match_id' value='{{$match_id}}'/>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Player Name</th>
                            <th>R</th>
                            <th>B</th>
                            <th>4s</th>
                            <th>6s</th>
                            <th>Over</th>
                            <th>No Ball</th>
                            <th>Wide</th>
                            <th>Maiden</th>
                            <th>Wicket</th>
                            <th>Runs Given</th>
                             <th>Down</th>
                            <th>Status</th>
                            <th>Under Eleven</th>
                        </tr>

                    </thead>
                    <tbody>

                        @foreach($final as $objKey => $objFinal) 
                        @foreach($objFinal as $objKeyOne => $objFinalOne)                    
                        @if($second_team_id == $objKeyOne)
                        @foreach($objFinalOne as $objKeySecond => $objFinalSecond)   
                        <?php
                        $run1 = (isset($records[$objKeyOne][$objKeySecond]['run']) && !empty($records[$objKeyOne][$objKeySecond]['run'])) ? $records[$objKeyOne][$objKeySecond]['run'] : "";
                        $ball1 = (isset($records[$objKeyOne][$objKeySecond]['ball']) && !empty($records[$objKeyOne][$objKeySecond]['ball'])) ? $records[$objKeyOne][$objKeySecond]['ball'] : "";
                        $fours1 = (isset($records[$objKeyOne][$objKeySecond]['four']) && !empty($records[$objKeyOne][$objKeySecond]['four'])) ? $records[$objKeyOne][$objKeySecond]['four'] : "";
                        $six1 = (isset($records[$objKeyOne][$objKeySecond]['six']) && !empty($records[$objKeyOne][$objKeySecond]['six'])) ? $records[$objKeyOne][$objKeySecond]['six'] : "";
                        $overs1 = (isset($records[$objKeyOne][$objKeySecond]['over']) && !empty($records[$objKeyOne][$objKeySecond]['over'])) ? $records[$objKeyOne][$objKeySecond]['over'] : "";
                        $noball1 = (isset($records[$objKeyOne][$objKeySecond]['noball']) && !empty($records[$objKeyOne][$objKeySecond]['noball'])) ? $records[$objKeyOne][$objKeySecond]['noball'] : "";
                        $wideball1 = (isset($records[$objKeyOne][$objKeySecond]['wideball']) && !empty($records[$objKeyOne][$objKeySecond]['wideball'])) ? $records[$objKeyOne][$objKeySecond]['wideball'] : "";
                        $maiden1 = (isset($records[$objKeyOne][$objKeySecond]['madian']) && !empty($records[$objKeyOne][$objKeySecond]['madian'])) ? $records[$objKeyOne][$objKeySecond]['madian'] : "";
                        $wicket1 = (isset($records[$objKeyOne][$objKeySecond]['wicket']) && !empty($records[$objKeyOne][$objKeySecond]['wicket'])) ? $records[$objKeyOne][$objKeySecond]['wicket'] : "";
                        $given_run1 = (isset($records[$objKeyOne][$objKeySecond]['given_run']) && !empty($records[$objKeyOne][$objKeySecond]['given_run'])) ? $records[$objKeyOne][$objKeySecond]['given_run'] : "";
                        $under_eleven1 = (isset($records[$objKeyOne][$objKeySecond]['under_eleven']) && !empty($records[$objKeyOne][$objKeySecond]['under_eleven'])) ? $records[$objKeyOne][$objKeySecond]['under_eleven'] : '0';
                        $ids1 = (isset($records[$objKeyOne][$objKeySecond]['id']) && !empty($records[$objKeyOne][$objKeySecond]['id'])) ? $records[$objKeyOne][$objKeySecond]['id'] : "";

                        $down1 = (isset($records[$objKeyOne][$objKeySecond]['down']) && !empty($records[$objKeyOne][$objKeySecond]['down'])) ? $records[$objKeyOne][$objKeySecond]['down'] : "";
                        $out_not1 = (isset($records[$objKeyOne][$objKeySecond]['out_not']) && !empty($records[$objKeyOne][$objKeySecond]['out_not'])) ? $records[$objKeyOne][$objKeySecond]['out_not'] : '0';


                        ?>
                        <tr>
                            <th>{{ $objFinalSecond }}</th>
                            <th> <div class="form-group">
                        {!! Form::text('runs['.$objKey . ']' . '['. $objKeySecond . ']', $run1 , 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Runs')) !!}
                    </div>
                    </th>
                    <th>
                    <input type='hidden' id='ids' name='ids[<?php echo $objKey; ?>][<?php echo $objKeySecond; ?>]' value='<?php echo $ids1 ?>'/>
                    <div class="form-group">
                        {!! Form::text('ball['.$objKey . ']' .'['. $objKeySecond . ']', $ball1, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Balls')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('fours['.$objKey . ']' .'['. $objKeySecond . ']', $fours1, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Fours')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('six['.$objKey . ']' .'['. $objKeySecond . ']', $six1, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Six')) !!}
                    </div>
                    </th>
<!--                    <th>
                    <div class="form-group">
                        {!! Form::text('strik_rate['.$objKey . ']' .'['. $objKeySecond . ']', null, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Strik Rate')) !!}
                    </div>
                    </th>-->
                    <th>
                    <div class="form-group">
                        {!! Form::text('overs['.$objKey . ']' .'['. $objKeySecond . ']', $overs1, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Overs')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('no_ball['.$objKey . ']' .'['. $objKeySecond . ']', $noball1, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'No Ball')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('wide['.$objKey . ']' .'['. $objKeySecond . ']', $wideball1, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Wide')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('maiden['.$objKey . ']' .'['. $objKeySecond . ']', $maiden1, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Maiden')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('wicket['.$objKey . ']' .'['. $objKeySecond . ']', $wicket1, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Wicket')) !!}
                    </div>
                    </th>
                    <th>
                    <div class="form-group">
                        {!! Form::text('runs_given['.$objKey . ']' .'['. $objKeySecond . ']', $given_run1, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Given Runs')) !!}
                    </div>
                    </th>

                    <th>
                    <div class="form-group">
                        {!! Form::text('down['.$objKey . ']' .'['. $objKeySecond . ']', $down1, 
                        array(
                        'class'=>'form-control', 
                        'placeholder'=>'Down')) !!}
                    </div>
                    </th>

                     <th>
                    <div class="form-group">
                        {!! Form::checkbox('out_not['.$objKey . ']' .'['. $objKeySecond . ']', $objKeySecond , $out_not1 , ['class' => 'form-control']) !!}                    
                    </div>
                    </th>

                     <th>
                    <div class="form-group">
                        {!! Form::checkbox('under_eleven[]', $objKeySecond , $under_eleven1 , ['class' => 'form-control']) !!}                    
                    </div>
                    </th>

                    </tr>

                    @endforeach
                    @endif    
                    @endforeach
                    @endforeach

                    </tbody>
                </table>  

                <div class="form-group" style="margin-left:50%;">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>


            <div class="tab-pane active" id="3">
                {!! Form::open(array('route' => 'admin.matchstatus', 'class' => 'form' , 'id' => 'three_form' , 'method' => 'post')) !!}
                <input type='hidden' name='first_team_id' value='{{$first_team_id}}'/>
                <input type='hidden' name='second_team_id' value='{{$second_team_id}}'/>
                <input type='hidden' name='match_id' value='{{$match_id}}'/>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    <div class="title">Match Status</div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="sub-title">First Umpires</div>
                                {!! Form::select('first-umpires', $umpires , $matchinfo['first_umpire'], array("class"=>"form-control", "style"=>"width:100%") ) !!}
                                
                                <div class="sub-title">Second Umpire</div>
                                {!! Form::select('second-umpires', $umpires , $matchinfo['second_umpire'], array("class"=>"form-control","style"=>"width:100%") ) !!}                                                                                                  
                                
                                <div class="sub-title"><?php echo @$first_team_name; ?></div>
                                <div>                                                                       
                                    <div>
                                        <label class="radio-inline">Total Run : <input type="text" id="team_run_<?php echo @$first_team_id; ?>" name="team_run[<?php echo @$first_team_id; ?>]" value="<?php echo @$matchinfo['first_team_run'] ?>"></label>
                                        <label class="radio-inline">Total Over: <input type="text" id="team_over_<?php echo @$first_team_id; ?>" name="team_over[<?php echo @$first_team_id; ?>]" value="<?php echo @$matchinfo['first_team_over'] ?>"></label>                                        
                                    </div>
                                </div>
                                <div class="sub-title"><?php echo @$second_team_name; ?></div>                                
                                <div>                                                                       
                                    <div>
                                        <div>
                                        <label class="radio-inline">Total Run : <input type="text" id="team_run_<?php echo @$second_team_id; ?>" name="team_run[<?php echo @$second_team_id; ?>]" value="<?php echo @$matchinfo['second_team_run'] ?>"></label>
                                        <label class="radio-inline">Total Over: <input type="text" id="team_over_<?php echo @$second_team_id; ?>" name="team_over[<?php echo @$second_team_id; ?>]" value="<?php echo @$matchinfo['second_team_over'] ?>"></label>                                        
                                    </div>                                        
                                    </div>
                                </div>
                                <div class="sub-title">Wining Team</div>
                                <div>                                                                       
                                    <div>
                                        <label class="radio-inline">
                                            <div class="iradio_flat" style="position: relative;"><input type="radio" value="<?php echo @$first_team_id; ?>" <?php if($first_team_id == $matchinfo['result']){echo "checked";} ?>  id="inlineRadio1" name="match_result" class="input-radio" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> <?php echo @$first_team_name; ?>
                                        </label>
                                        <label class="radio-inline">
                                            <div class="iradio_flat" style="position: relative;"><input type="radio" value="<?php echo @$second_team_id; ?>" <?php if($second_team_id == $matchinfo['result']){echo "checked";} ?> id="inlineRadio2" name="match_result" class="input-radio" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> <?php echo @$second_team_name; ?>
                                        </label>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-left:50%;">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
                {!! Form::close() !!}           

            </div>
        </div>

    </div> 


</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#3').hide();

        $('#three').click(function() {
            $('#3').show();
        });
        $('#one').click(function() {
            $('#3').hide();
        });

    });

</script>
@endsection

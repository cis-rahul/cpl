@extends('admin.layouts.master')

@section('content')

<div class="side-body padding-top">
    <ul>
        @if(Session::has('message'))
                <p style="color: green; font-size: 15px;" class="">{{ Session::get('message') }}</p>
                @endif

        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">

                    <div class="card-title" style="width:100%">
                        <div class="title">List of Player's    
                              <a href="{{ route("admin.player") }}">
                                    <span style="color:green; float:right; font-size:15px;" class="title">Add Player</span>
                              </a>
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <table class="datatable table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Player Name</th>
                                <th>Team Name</th>
                                <th>Player Type</th>
                                <th>Player DP</th>
                                <th>Created Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>

                             @foreach($player_data as $data)

                            <tr>
                                <th>{{$data->id}}</th>
                                <th>{{$data->player_name}}</th>
                                <th>{{$data->team_name}}</th>
                                <th>{{$data->player_type}}</th>
                                <th><img style="width:100px; border:1px solid #CCCCCC; border-radius:5px;height:80px;" src="uploads/{{$data->player_photo}}" /></th> 
                                <th>{{$data->created_at}}</th>
                                <th>
                                    <a alt="update" href="{{ route('admin.editplayer', $data->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;&nbsp;&nbsp;
                                    <a alt="delete" href="{{route('admin.deleplayer', $data->id)}}"> <span class="glyphicon glyphicon-remove"></span></a>
                                </th>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection

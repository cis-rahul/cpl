@extends('admin.layouts.master')

@section('content')
  {!! HTML::script('http://code.jquery.com/jquery-1.10.2.js') !!}
  {!! HTML::script('http://code.jquery.com/ui/1.11.4/jquery-ui.js') !!}
  
<div class="side-body padding-top">
    <ul>
        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
    
    <h4><i>Select Teams To Schedule Match</i> <a href="{{ route("admin.allmatches") }}">
        <span style="color:green; float:right; font-size:15px;" class="title">List Matches</span>
    </a> </h4>


    {!! Form::open(array('route' => 'admin.matchadd', 'class' => 'form' , 'id' => 'save_player' , 'method' => 'post')) !!}
 
   <div class="form-group">
        {!! Form::label('Select First Team') !!}<br />
        {!! Form::select('team_name_first', $team_data,'null', array("class"=>"form-control") ) !!}
    </div>

  <div class="form-group">
        {!! Form::label('Select Second Team') !!}<br />
        {!! Form::select('team_name_second', $team_data,'null', array("class"=>"form-control") ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('Pick a Date') !!}<br />
        {!! Form::text('datepicker', '', array('class'=>'form-control','placeholder'=>'Pick date please' , 'id'=>'datepicker')) !!}
    </div>
 
    <div class="form-group">
        {!! Form::submit('Add', 
        array('class'=>'btn btn-primary')) !!}
    </div>
    {!! Form::close() !!}
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $( "#datepicker" ).datepicker();     
    });
     
</script>

@endsection



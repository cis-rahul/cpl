@extends('front.layouts.master')
@section('content')
<style type="text/css">
    .img{width:65% !important; height: 100px;}
    .text{font-size:12px;}
    .no_img{ margin: 0 0 0 66%;
             padding: 28% 0 0; }
    </style> 
    <section id="summary-slider">
    <div class="general">
        <div class="content-result content-result-news col-md-12">
            <div id="textslide" class="effect-backcolor">
                <div class="container">
                    <div class="col-md-12 slide-txt">
                        <p class='sub-result aft-little welcome linetheme-left'>THE WAR</p>
                        <p class='sub-result aft-little linetheme-right'>2016 - CPL<span class="point-big">.</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div id="slidematch" class="col-xs-12 col-md-12">
            <div class="content-match-team-wrapper">
                <span class="gdlr-left">Cyber Infrastructure</span>
                <span class="gdlr-upcoming-match-versus"></span>
                <span class="gdlr-right">Premier League</span>
            </div>
            <div class="content-match-team-time">
                <span class="gdlr-left">Starts From 1 April 2016</span>
                <span class="gdlr-right">DAVV</span>
            </div>
        </div>
    </div>
</section>

<section id="atp-match">
    <div class="container">
        <div id="people-top" class="top-match col-xs-12 col-md-12">
            <h3 style="color:lightyellow;" class="news-title n-match"><span> Five Best  </span>  Teams & Players <span class="point-little">.</span></h3>
            <p class="subtitle" style="color:lightyellow;">A small creative team, trying to enrich the lives of others and build brands 
                that a normal humans can understand.</p>
            <!--SECTION ATP MATCH-->

            <div class="col-md-4 home-page">
                <div class="main">
                    <div class="tabs animated-slide-2">
                        <div class="result-filter">
                            <ul class="tab-links">
                                <li class="active"><a href="#tab1111">Batsman</a></li>
                                <li><a href="#tab2222">Bowller</a></li>
                                <li><a href="#tab3333">Points-Table</a></li>
                            </ul>
                        </div>
                        <div class="tab-content-point">
                            <div id="tab1111" class="tab active">
                                <table class="tab-score">
                                    <tr class="top-scrore-table"><td class="score-position">POS.</td><td>PLAYER</td><td>TEAM</td><td>RUN</td></tr>                                    
                                    <?php
                                       $strHtml = "";                                       
                                       if(!empty($top_five_bastman)){
                                           $i = 1;
//                                           echo "<pre>";
//                                           print_r($top_five_bastman);die;                                           
                                           foreach($top_five_bastman as $objBastman){                                               
//                                               echo "<pre>";
//                                               print_r($objBastman);die;
                                               $strHtml .= "<tr><td class='score-position'>".$i.".</td><td><a href='".route('front.cplplayerdetail', $objBastman->player_id)."'>".$objBastman->player_name."</a></td><td>".HTML::image("uploads/" . $objBastman->team_logo, 'picture')."</td><td>".$objBastman->total_run."</td></tr>";
                                               $i++;
                                           }
                                           echo $strHtml;
                                       }
                                    ?>                                                                                                                                                

                                </table>
                            </div>
                            <div id="tab2222" class="tab">
                                <table class="tab-score">
                                    <tr class="top-scrore-table"><td class="score-position">POS.</td><td>PLAYER</td><td>TEAM</td><td>WICKET</td></tr>
                                    <?php
                                       $strHtml = "";                                       
                                       if(!empty($top_five_bowler)){
                                           $i = 1;                                           
                                           foreach($top_five_bowler as $objBowller){
                                               $strHtml .= "<tr><td class='score-position'>".$i.".</td><td><a href='single_player.html'>".$objBowller->player_name."</a></td><td>".HTML::image("uploads/" . $objBowller->team_logo , 'picture')."</td><td>".$objBowller->total_wicket."</td></tr>";
                                               $i++;
                                           }
                                           echo $strHtml;
                                       }
                                    ?>
                                </table>
                            </div>
                            <div id="tab3333" class="tab">
                                <table class="tab-score">
                                    <tr class="top-scrore-table"><td class="score-position">POS.</td><td>TEAM</td><td>POINTS</td><td>NAT.</td></tr>
                                    <?php
                                       $strHtml = "";                                       
                                       if(!empty($top_five_team)){
                                           $i = 1;                                           
                                           foreach($top_five_team as $objTeams){
//                                               echo "<pre>";
//                                               print_r($objTeams);die;
                                               if($i <= 5){
                                                $strHtml .= "<tr><td class='score-position'>{$i}.</td><td><a href='single_player.html'>".HTML::image("uploads/" . $objTeams['team_logo'] , 'picture')."</a></td><td>{$objTeams['point']}</td><td>{$objTeams['avg']}</td></tr>";
                                               }
                                               $i++;
                                           }
                                           echo $strHtml;
                                       }
                                    ?>                                    
                                </table>
                            </div>
                        </div>
                        <div class="score-view-all"><a class="pl-point-button" href="{{route('front.cpltopplayers')}}">View All</a></div>
                    </div>
                </div>
            </div>
        </div><!--Close Top Match-->
    </div>
</section>

<!--SECTION NEXT MATCH-->
<section id="next-match">
    <div  class="container">
        <div class="next-match-news top-match col-xs-12 col-md-12">
            <h3 class="news-title n-match">TODAY'S <span>Match</span><span class="point-little">.</span></h3>
            <p class="subtitle">A small creative team, trying to enrich the lives of others and build brands 
                that a normal humans can understand.</p>

            @if (isset($match_data[0]) && !empty($match_data[0]))
           
            <div class="other-match col-md-4">
                <div class="score-next-time">
                    <div class="circle-ico"><p>CPL</p></div>
                </div>
                <div class="col-xs-5 col-md-5 match-team">
                    {!! HTML::image('uploads/'. $match_data[0]->t1_team_logo, 'Team First' , array('class' => 'img')) !!}
                    <p class="text">{!! $match_data[0]->t1_team_name !!}</p>
                </div>
                <div class="col-xs-2 col-md-2 match-team-vs">
                    <span class="txt-vs"> - vs - </span>
                </div>
                <div class="col-xs-5 col-md-5 match-team">
                    {!! HTML::image('uploads/'. $match_data[0]->t2_team_logo ,'Team Second', array('class' => 'img')) !!}
                    <p class="text">{!! $match_data[0]->t2_team_name !!}</p>
                </div>
                <div class="next-match-place">
                    <p class='sub-result'>FIRST MATCH</p>
                    <p class="dd-news-date">{!! date("d, M Y", strtotime($match_data[0]->match_date)) !!} - 6:00 AM</p>
                </div>
 
            </div>
            @else
            <div class="other-match col-md-4">
                <div class="score-next-time">
                    <div class="circle-ico"><p>CPL</p></div>
                </div> 
                <div class="col-xs-5 col-md-5 match-team">
                    {!! HTML::image('front-theme/images/nodata.png', 'picture' , array('class'=>'no_img' )) !!}
                    <p class="text">NO MATCH TODAY</p>
                </div>
            </div>
            @endif
            
            <div class="other-match col-md-4">
                {!! HTML::image('front-theme/images/adwertise.jpg', 'picture') !!}
            </div>
            
            @if (isset($match_data[1]) && !empty($match_data[1]))
            
            <div class="other-match col-md-4 other-last">                 
                <div class="score-next-time">
                    <div class="circle-ico"><p>CPL</p></div>
                </div>
                <div class="col-xs-5 col-md-5 match-team">
                   
                    {!! HTML::image('uploads/'. $match_data[1]->t1_team_logo ,'Team First' ,  array('class' => 'img')) !!}                    
                    
                    <p class="text">{!! $match_data[1]->t1_team_name !!}</p>
                    
                    
                </div>
                <div class="col-xs-2 col-md-2 match-team-vs">
                    <span class="txt-vs"> - vs - </span>
                </div>
                <div class="col-xs-5 col-md-5 match-team">
                    {!! HTML::image('uploads/'. $match_data[1]->t2_team_logo ,'Team Second' ,  array('class' => 'img')) !!}
                    <p class="text">{!! $match_data[1]->t2_team_name !!}</p>
                </div>
                <div class="next-match-place">
                    <p class='sub-result'>SECOND MATCH</p>
                    <p class="dd-news-date">{!! date("d, M Y", strtotime($match_data[1]->match_date)) !!} - 7:15 AM </p>
                </div>                
            </div>
            @else
            <div class="other-match col-md-4 other-last">
                <div class="score-next-time">
                    <div class="circle-ico"><p>CPL</p></div>
                </div> 
                <div class="col-xs-5 col-md-5 match-team">
                    {!! HTML::image('front-theme/images/nodata.png', 'picture' , array('class'=>'no_img' )) !!}
                    <p class="text">NO MATCH TODAY</p>
                </div>
            </div>
            @endif                                                                             

        </div>
    </div>
</section>


<!-- PARALLAX BLACK TENNIS-->

<!--SECTION STATISTIC RESULTS-->
<section id="resultsPoint">
    <div class="container">
        <div class="top-match col-xs-12 col-md-12">
            <div class="top-score-title">
                <h3>Statistics<span class="point-little">.</span></h3>
                <p class="subtitle">A small creative team, trying to enrich the lives of others and build brands 
                    that a normal humans can understand.</p>

                <div class="clear"></div>

            </div>
        </div>
    </div>
</section>

<section id="parallax-info">
    <div class="clearfix">
        <div class="col-md-6 yoga-desc title">
            <h1>Robert Doe</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sed ligula odio. Sed id metus felis. Ut pretium nisl non justo condimentum id tincidunt nunc faucibus. Ut neque eros, pulvinar eu blandit quis, lacinia nec ipsum. Etiam vel orci ipsum. Sed eget velit ipsum. 
                Duis in tortor scelerisque felis mattis imperdiet. Donec at libero tellus. Suspendisse consectetur consectetur bibendum.</p>

            <p class="txt-break">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sed ligula odio. Sed id metus felis. Ut pretium nisl non justo condimentum id tincidunt nunc faucibus. Ut neque eros, pulvinar eu blandit quis, lacinia nec ipsum. Etiam vel orci ipsum. Sed eget velit ipsum. 
                Duis in tortor scelerisque felis mattis imperdiet. Donec at libero tellus. Suspendisse consectetur consectetur bibendum.</p>
            {!! HTML::image('front-theme/images/signature.png', 'picture' , array('class'=>'signature' )) !!}

        </div>
        <div class="col-md-6 cup-img" style="padding:0;">
            {!! HTML::image('front-theme/images/bats.jpg', 'bats') !!}
        </div>      
    </div>
</section>


@endsection


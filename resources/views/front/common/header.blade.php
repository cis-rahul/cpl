<section class="container box-logo">
    <header>
        <div class="content-logo col-md-12">
            <div class="logo">
                {!! HTML::image('front-theme/img/logo2.png', 'logo',array('width' => 177)) !!}
            </div>

            <div class="bt-menu"><a href="#" class="menu"><span>&equiv;</span> Menu</a></div>

            <div class="box-menu">

                <nav id="cbp-hrmenu" class="cbp-hrmenu">
                    <ul id="menu">
                        <li><a class="lnk-menu active" href="{{ route('admin.homepage') }}">Home</a></li>
                        <li><a class="lnk-menu" href="{{ route('front.cplplayer') }}">Players</a></li>
                        <li><a class="lnk-menu" href="{{ route('front.cplschedule') }}">Schedule</a></li>
                        <!-- <li><a class="lnk-menu" href="{{ route('front.cplscorecard') }}">Results</a></li> -->
                    </ul>
                </nav>
            </div>
        </div>
    </header>
</section>
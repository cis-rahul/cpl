
        <footer>
            <div class="col-md-12 content-footer">
                <p>©2016 CPL All rights reserved. </p>
            </div>
        </footer>


        {!! HTML::script('front-theme/js/jquery-1.10.2.js') !!}
        {!! HTML::script('front-theme/js/jquery-migrate-1.2.1.min.js') !!}
        {!! HTML::script('front-theme/js/jquery.transit.min.js') !!}
        {!! HTML::script('front-theme/js/menu/cbpHorizontalMenu.js') !!}
        {!! HTML::script('front-theme/js/menu/modernizr.custom.js') !!}

        {!! HTML::script('front-theme/js/minislide/jquery.flexslider.js') !!}
        {!! HTML::script('front-theme/js/circle/jquery-asPieProgress.js') !!}
        {!! HTML::script('front-theme/js/circle/js/circle/rainbow.min.js') !!}

        {!! HTML::script('front-theme/js/gallery/jquery.prettyPhoto.js') !!}
        {!! HTML::script('front-theme/js/gallery/isotope.js') !!}

        {!! HTML::script('front-theme/js/jquery.ui.totop.js') !!}
        {!! HTML::script('front-theme/js/custom.js') !!}

        {!! HTML::script('front-theme/js/jquery.bxslider.js') !!}
        {!! HTML::script('front-theme/js/jquery.easing.1.3.js') !!}
        {!! HTML::script('front-theme/js/jquery.mousewheel.js') !!}

        {!! HTML::script('front-theme/js/own/owl.carousel.js') !!}
        {!! HTML::script('front-theme/js/jquery.countdown.js') !!}
        {!! HTML::script('front-theme/js/custom_ini.js') !!}

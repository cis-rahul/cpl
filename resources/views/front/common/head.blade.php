<head>
        <title>@yield('title')</title>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />

        <meta name="author" content="Corsini Alessio" />
        <meta name="keywords" content="Tennis, club, events, football, golf, non-profit, betting assistant, football,fitness, tennis, sport, soccer, goal, sports, volleyball, basketball,  charity, club, cricket, football, hockey, magazine, non profit, rugby, soccer, sport, sports, tennis" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        {!! HTML::style('front-theme/css/bootstrap.css') !!}
        {!! HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300') !!}
        {!! HTML::style('http://fonts.googleapis.com/css?family=Raleway:400,100,300,200,500,600,700,800,900') !!}        
        {!! HTML::style('http://fonts.googleapis.com/css?family=Oswald:400,300,700') !!}        
        {!! HTML::style('front-theme/css/fonts/font-awesome-4.1.0/css/font-awesome.min.css') !!}        
        {!! HTML::style('front-theme/css/fonts/font-awesome-4.1.0/css/font-awesome.min.css') !!}
        {!! HTML::style('front-theme/css/own/owl.carousel.css') !!}
        {!! HTML::style('front-theme/css/own/owl.theme.css') !!}
        {!! HTML::style('front-theme/css/jquery.bxslider.css') !!}
        {!! HTML::style('front-theme/css/jquery.jscrollpane.css') !!}
        {!! HTML::style('front-theme/css/minislide/flexslider.css') !!}
        {!! HTML::style('front-theme/css/component.css') !!}
        {!! HTML::style('front-theme/css/prettyPhoto.css') !!}
        {!! HTML::style('front-theme/css/style_dir.css') !!}            
        <link rel="shortcut icon" type="image/png" href="img/favicon.ico" />
        {!! HTML::style('front-theme/css/responsive.css') !!}            
        {!! HTML::style('front-theme/css/animate.css') !!}            
    </head>

 
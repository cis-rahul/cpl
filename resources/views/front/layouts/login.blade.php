<!DOCTYPE html>
<html>
    @include('admin.common.head')
<head>
    <title>Login</title>
</head>

<body class="flat-blue login-page">

    <div class="container">
        <div class="login-box">
 <ul>
        @if(Session::has('message'))
                <p style="color:red; font-size: 15px;" class="">{{ Session::get('message') }}</p>
                @endif

        @foreach($errors->all() as $error)
        <li style="color:red;" >{{ $error }}</li>
        @endforeach
    </ul>
 
                <div class="login-form row">
                    <div class="col-sm-12 text-center login-header">
                        <i class="login-logo fa fa-connectdevelop fa-5x"></i>
                        <h4 class="login-title">Admin CPL - 2016</h4>
                    </div>
                    <div class="col-sm-12">
                        <div class="login-body">
                            <div class="progress hidden" id="login-progress">
                                <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                    Log In...
                                </div>
                            </div>


                            {!! Form::open(array('route'=>'admin.checklogin','class'=>'form')) !!}

                            <div class="form-group">
                                {!! Form::label('E-mail') !!}
                                {!! Form::text('email', null, 
                                    array('required', 
                                          'class'=>'form-control', 
                                          'placeholder'=>'admin@gmail.com')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('Password') !!}
                                {!! Form::input('password', 'password', null, 
                                    array('required', 
                                          'class'=>'form-control', 
                                          'placeholder'=>'Password')) !!}
                            </div>

                            <div class="login-button text-center">
                                {!! Form::submit('Login', 
                                  array('class'=>'btn btn-primary')) !!}
                            </div>

                            {!! Form::close() !!}

                            
                         </div>
                    </div>
                </div>
         @include('admin.common.footer')
    </div>
     </div>
     
</body>

</html>

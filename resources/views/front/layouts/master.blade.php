<!DOCTYPE html>
<html>
    @include('front.common.head')
    <body class="flat-blue">
        <div class="app-container">
            <div class="row content-container">
                @include('front.common.header')
                @yield('content')

            </div>
            @include('front.common.footer')
        </div>   
    </body>
</html>


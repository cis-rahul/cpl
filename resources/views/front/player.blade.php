@extends('front.layouts.master')

@section('content')
    <style type="text/css">
    .atp-player p{
        margin-top: -16px !important;
    }
    .atp-player p {
      background-color: rgba(0, 0, 0, 0.8);
      color: #fff;
      font-size: 10px;
      margin-top: -50px;
      padding: 5px;
      position: absolute;
      text-transform: none;
}
img {
  border: 2px solid #8a8a8a;
  width: 100%;
  height: 100%;
}
    </style>

    <section class="drawer">
        <div class="col-md-12 size-img back-img-match">
            <div class="effect-cover">
                <h3 class="txt-advert animated">Team & Players</h3>
            </div>
        </div>
     
    <section id="players" class="container secondary-page">
      <div class="general general-results players">
              
           <div class="top-score-title right-score col-md-12">

                 @foreach($all_data as $data)                          
                    <div class="content-wtp-player">
                        <h3>{!! $data[0]['team_name'] !!} <span>Players</span><span class="point-little">.</span></h3>
                        
                        @foreach($data as $player_deatail)

                        <div class="col-md-3 atp-player">
                        <div class="player_pic" style="width:100px; height:100px;">
                          <a href="{{route('front.cplplayerdetail', $player_deatail['player_id']) }}">{!! HTML::image('uploads/'. $player_deatail['player_photo'] , 'picture') !!}
                        </div>  
                          <p>{!! $player_deatail['player_name'];  !!}</p></a>
                        </div>
                        @endforeach
                    </div>
                 @endforeach
             
           </div><!--Close Top Match-->
        </section>
        

@endsection


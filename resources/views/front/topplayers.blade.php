@extends('front.layouts.master')
@section('content')
<style type="text/css">
    #boll1 {margin:41% 0 0;}
</style>

<?php
    $batsmanpos = "";
    $bastmanplayername = "";
    $bastmanteamname = "";
    $bastmanrun = "";
    $bastmanteamlogo = "";
    if(!empty($top_batsman)){        
        $i = 1;        
        foreach($top_batsman as $objTopBatsman){ 
            if($objTopBatsman->total_run > 0){
                $batsmanpos .= "<p style='height:40px;'>{$i}</p>";
                $bastmanplayername .= "<p style='height:40px;'>{$objTopBatsman->player_name}</p>";
                $bastmanteamlogo .= "<p style='height:40px;'><img width='20px' alt='picture' src='uploads/{$objTopBatsman->team_logo}'></p>";
                $bastmanrun .= "<p style='height:40px;'>{$objTopBatsman->total_run}</p>";
                $bastmanteamname .= "<p style='height:40px;'>{$objTopBatsman->team_name}</p>";
                $i++;
            }
        }        
    }
?>
<?php
    $ballpos = "";
    $ballplayername = "";
    $ballteamname = "";
    $ballwicket = "";
    $ballteamlogo = ""; 
    $ballrungiven = "";
    $ballover = "";
    if(!empty($top_bowller)){        
        $i = 1;        
        foreach($top_bowller as $objTopBowller){
            if($objTopBowller->total_over > 0){
                $ballpos .= "<p style='height:40px;'>{$i}</p>";
                $ballplayername .= "<p style='height:40px;'>{$objTopBowller->player_name}</p>";
                $ballteamlogo .= "<p style='height:40px;'><img width='20px' alt='picture' src='uploads/{$objTopBowller->team_logo}'></p>";
                $ballwicket .= "<p style='height:40px;'>{$objTopBowller->total_wicket}</p>";
                $ballteamname .= "<p style='height:40px;'>{$objTopBowller->team_name}</p>";
                $ballrungiven .= "<p style='height:40px;'>{$objTopBowller->total_given_run}</p>";
                $ballover .= "<p style='height:40px;'>{$objTopBowller->total_over}</p>";
                $i++;
            }
        }        
    }    
?>
<?php
    $teampos = "";
    $teamlogo = "";
    $teampoints = "";
    $teamavg = "";
    $teamname = "";     
    if(!empty($top_teams)){        
        $i = 1;        
        foreach($top_teams as $objTopTeams){            
            $teampos .= "<p style='height:40px;'>{$i}</p>";
            $teampoints .= "<p style='height:40px;'>{$objTopTeams['point']}</p>";
            $teamlogo .= "<p style='height:40px;'><img width='20px' alt='picture' src='uploads/{$objTopTeams['team_logo']}'></p>";
            $teamavg .= "<p style='height:40px;'>{$objTopTeams['avg']}</p>";
            $teamname .= "<p style='height:40px;'>{$objTopTeams['team_name']}</p>";
            $i++;            
        }        
    }    
?>



<section class="drawer">
    <div class="col-md-12 size-img back-img-single">
        <div class="effect-cover">
            <h3 class="txt-advert animated">PLAYERS</h3>
            <p class="txt-advert-sub">Performance</p>
        </div>
    </div>

    <section id="single-match-pl" class="container secondary-page">
        <div class="general general-results players">   
            <div class="top-score-title right-score col-md-12">
                <div class="top-score-title player-vs">
                    <div class="main">
                        <div class="tabs standard single-pl">

                            <div class="tab-content single-match">
                                <div id="tab1" class="tab active">
                                    <h3 class="tab-match-title">PLAYERS STATISTICS</h3>
                                    <div class="tabs standard">
                                        <ul class="tab-links-match">
                                            <li class="active"><a class="first-tabs" href="#tab1a">  Top Batsman</a></li>
                                            <li><a href="#tab2a">Top Bowller</a></li>
                                            <li><a href="#tab3a">Top Teams</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab active" id="tab1a">                                                
                                                <div class="col-md-1 single-match-data">
                                                    <p class="nm-player">POS.</p>
                                                    <?php echo @$batsmanpos ?>
                                                </div>
                                                <div class="col-md-3 single-match-data">
                                                    <p class="nm-player">Player</p>
                                                    <?php echo @$bastmanplayername ?>                                                    
                                                </div>
                                                <div class="col-md-3 single-match-data">
                                                    <p class="nm-player">Team Name </p>
                                                    <?php echo $bastmanteamname; ?>                                                    
                                                </div>

                                                <div class="col-md-2 single-match-data">
                                                    <p class="nm-player">Run </p>
                                                    <?php echo @$bastmanrun ?> 
                                                </div>

                                                <div class="col-md-1 single-match-data">
                                                    <p class="nm-player">Team Logo </p>
                                                    <?php echo @$bastmanteamlogo ?>
                                                </div>                                                
                                            </div>
                                            <div class="tab" id="tab2a">                                                
                                                <div class="tab active" id="tab1a">
                                                    <div class="col-md-1 single-match-data">
                                                        <p class="nm-player">POS.</p>
                                                        <?php echo @$ballpos ?>
                                                    </div>
                                                    <div class="col-md-2 single-match-data">
                                                        <p class="nm-player">Player Name</p>
                                                        <?php echo @$ballplayername ?>
                                                    </div>
                                                    <div class="col-md-2 single-match-data">
                                                        <p class="nm-player">Team Name </p>
                                                        <?php echo @$ballteamname ?>
                                                    </div>                                                    
                                                    <div class="col-md-1 single-match-data">
                                                        <p class="nm-player">Overs </p>
                                                        <?php echo @$ballover ?>
                                                    </div>
                                                    
                                                    <div class="col-md-1 single-match-data">
                                                        <p class="nm-player">Given Run's </p>
                                                        <?php echo @$ballrungiven ?>
                                                    </div>
                                                    <div class="col-md-1 single-match-data">
                                                        <p class="nm-player">Wicket </p>
                                                        <?php echo @$ballwicket ?>
                                                    </div>
                                                    <div class="col-md-1 single-match-data">
                                                        <p class="nm-player">Team Logo </p>
                                                        <?php echo @$ballteamlogo ?>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            <div class="tab" id="tab3a">                                                
                                                <div class="tab active" id="tab1a">
                                                    <div class="col-md-1 single-match-data">
                                                        <p class="nm-player">POS.</p>
                                                        <?php echo @$teampos ?>
                                                    </div>
                                                    <div class="col-md-2 single-match-data">
                                                        <p class="nm-player">Team Name</p>
                                                        <?php echo @$teamname ?>
                                                    </div>
                                                    <div class="col-md-2 single-match-data">
                                                        <p class="nm-player">Teams</p>
                                                        <?php echo @$teamlogo ?>
                                                    </div>                                                    
                                                    <div class="col-md-1 single-match-data">
                                                        <p class="nm-player">NET. </p>
                                                        <?php echo @$teamavg ?>
                                                    </div>
                                                    
                                                    <div class="col-md-1 single-match-data">
                                                        <p class="nm-player">Points </p>
                                                        <?php echo @$teampoints ?>
                                                    </div>                                                                                                       
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
                                </div>                                 
                            </div>
                        </div> 
                    </div>
                </div><!--Close Top Match-->
                </section> 
                @endsection


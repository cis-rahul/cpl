﻿@extends('front.layouts.master')

@section('content')
<style type="text/css">
    #section1{
        margin-top: 2%;
    }
</style>

 <section class="container box-logo">
   
    <div class="col-md-12 size-img back-img">
        <div class="effect-cover">
        <h3 class="txt-advert animated">2016 CPL</h3>
        <p class="txt-advert-sub animated">SCHEDULE</p>
        </div>
    </div>
   
      <div class="general general-results tournaments">
           
           <div id="c-calend" class="top-score-title right-score col-md-12">
                <div class="accordion"  id="section1"><i class="fa fa-calendar-o"></i>APRIL<span></span></div>
                    <div class="acc-content">
                            <div class="col-md-1 acc-title">S.no.</div>
                            <div class="col-md-2 acc-title">Match Date</div>
                            <div class="col-md-2 acc-title">First Team </div>
                            <div class="col-md-2 acc-title">Second Team</div>
                            <div class="col-md-2 acc-title">Empires</div>
                            <div class="col-md-2 acc-title">Result</div>
                            <div class="col-md-1 acc-title">Scorecard</div>
                            
                     <div class="acc-footer"></div>                     
                            @foreach($match_data as $data)
                            <div class="col-md-1 timg">{{ $data->match_id }}</div>
                            <div class="col-md-2 t3"><p>{{ $data->match_date }}</p></div>
                            <div class="col-md-2 t1"><p>{{ $data->t1_team_name }}</p></div>
                            <div class="col-md-2 t2"><p>{{ $data->t2_team_name }}</p> </div>
                            <div class="col-md-2 t2"><p>@if(isset($data->first_umpire_name) && $data->second_umpire_name){{ $data->first_umpire_name .'/'. $data->second_umpire_name  }} @endif</p> </div>
                            <div class="col-md-2 t2"><p>{{ $data->t3_team_name }}</p> </div>
                            <div class="col-md-1 t2"><a href="{{ route('front.cplscorecard' ,  $data->match_id) }}"><p>@if(isset($data->first_umpire_name)) View  @endif</p></a> </div>
                            <div class="acc-footer"></div>
                            @endforeach
            </div>
            </div>
                      
              
              
       </div>
            
   
</section>
        
@endsection
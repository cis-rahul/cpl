@extends('front.layouts.master')
@section('content')

<style type="text/css">
  #boll1 {margin:41% 0 0;}
</style>>
    <section class="drawer">
            <div class="col-md-12 size-img back-img-single">
                <div class="effect-cover">
                    <h3 class="txt-advert animated">SCORECARD</h3>
                    <p class="txt-advert-sub">MATCH</p>
                </div>
           </div>
  
    <section id="single-match-pl" class="container secondary-page">
      <div class="general general-results players">   
           <div class="top-score-title right-score col-md-12">
                <div class="top-score-title player-vs">
                        <div class="main">
                            <div class="tabs standard single-pl">
                                
                                <div class="tab-content single-match">
                                    <div id="tab1" class="tab active">
                                       <h3 class="tab-match-title">MATCH STATISTICS</h3>
                                        <div class="tabs standard">
                                            <ul class="tab-links-match">

                                                <li class="active"><a class="first-tabs" href="#tab1a">  {{ $score_data[0]->t1_team_name }}</a></li>
                                                <li><a href="#tab2a">{{ $score_data[15]->t1_team_name }}</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="tab1a" class="tab active">
                                                <h4>Batting</h4>
                                                   <div class="col-md-3 single-match-data">
                                                      <p class="nm-player">Player Name</p>
                                                      <?php
                                                      $teamid = '0';
                                                      $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row){
                                                        if($i == 1 || $teamid == $row->team_id){
                                                        $str .= '<p>' . $row->p1_player_name . '</p>';
                                                        }else{
                                                          break;
                                                        }
                                                        $teamid = $row->team_id;
                                                        $i++;
                                                      }
                                                      echo $str;
                                                     ?>
                                                    
                                                   </div>
                                                   
                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Run</p>
                                                     <?php
                                                      $teamid = '0';
                                                      $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row){
                                                        if($i == 1 || $teamid == $row->team_id){
                                                        $str .= '<p>' . $row->run . '</p>';
                                                        }else{
                                                          break;
                                                        }
                                                        $teamid = $row->team_id;
                                                        $i++;
                                                      }
                                                      echo $str;
                                                     ?>
                                                    
                                                   </div>


                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Ball </p>
                                                     <?php
                                                      $teamid = '0';
                                                      $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row){
                                                        if($i == 1 || $teamid == $row->team_id){
                                                        $str .= '<p>' . $row->ball . '</p>';
                                                        }else{
                                                          break;
                                                        }
                                                        $teamid = $row->team_id;
                                                        $i++;
                                                      }
                                                      echo $str;
                                                     ?>
                                                    
                                                   </div>

                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Four </p>
                                                     <?php
                                                      $teamid = '0';
                                                      $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row){                                                        
                                                        if($i == 1 || $teamid == $row->team_id){
                                                        $str .= '<p>' . $row->four . '</p>';
                                                        }else{
                                                          break;
                                                        }
                                                        $teamid = $row->team_id;
                                                        $i++;
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>

                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Six </p>
                                                     <?php
                                                      $teamid = '0';
                                                      $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row){
                                                        if($i == 1 || $teamid == $row->team_id){
                                                        $str .= '<p>' . $row->six . '</p>';
                                                        }else{
                                                          $teamid = $row->team_id;
                                                          break;
                                                        }
                                                        $teamid = $row->team_id;
                                                        $i++;
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>


                                                      <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Player Status </p>

                                                      <?php
                                                      $teamid = '0';

                                                      $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row){
                                                        if($i == 1 || $teamid == $row->team_id){

                                                        $out_not = '';
                                                        if($row->out_not == '1'){ $out_not = 'Out' ; } else{ $out_not = 'Not Out'; }

                                                        $str .= '<p>' . $out_not . '</p>';
                                                        }else{
                                                          $teamid = $row->team_id;
                                                          break;
                                                        }
                                                        $teamid = $row->team_id;
                                                        $i++;
                                                      }
                                                      echo $str;
                                                     ?>

                                                   </div>

                                                

                                                <div id="boll1" class="tab active">
                                                <h4>Bowlling</h4>
                                                   <div class="col-md-3 single-match-data">
                                                      <p class="nm-player">Player Name</p>
                                                      <?php                                                      
                                                      $str = "";                                                      
                                                      foreach($score_data as $row1){
                                                        if($teamid == $row1->team_id && $row1->over > 0.0){
                                                             $str .= '<p>' . $row1->p1_player_name . '</p>';
                                                        }
                                                        $teamid = $row->team_id;
                                                      }
                                                      echo $str;
                                                     ?>
                                                    
                                                   </div>
                                                   
                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Over</p>
                                                    <?php                                                      
                                                      $str = "";                                                      
                                                      foreach($score_data as $row1){
                                                        if($teamid == $row1->team_id && $row1->over > 0.0){
                                                             $str .= '<p>' . $row1->over . '</p>';
                                                        }
                                                        $teamid = $row->team_id;
                                                      }
                                                      echo $str;
                                                     ?>
                                                    
                                                   </div>


                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Run </p>
                                                     <?php
                                                      // $teamid = '0';
                                                      // $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row1){
                                                        if($teamid == $row1->team_id && $row1->over > 0.0){
                                                             $str .= '<p>' . $row1->given_run . '</p>';
                                                        }
                                                        $teamid = $row->team_id;
                                                      }
                                                      echo $str;
                                                     ?>
                                                    
                                                   </div>

                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Wide </p>
                                                    <?php
                                                      // $teamid = '0';
                                                      // $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row1){
                                                        if($teamid == $row1->team_id && $row1->over > 0.0){
                                                             $str .= '<p>' . $row1->wideball . '</p>';
                                                        }
                                                        $teamid = $row->team_id;
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>

                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Maiden </p>
                                                    <?php
                                                      // $teamid = '0';
                                                      // $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row1){
                                                        if($teamid == $row1->team_id && $row1->over > 0.0){
                                                             $str .= '<p>' . $row1->madian . '</p>';
                                                        }
                                                        $teamid = $row->team_id;
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>


                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Wicket </p>
                                                     <?php
                                                      // $teamid = '0';
                                                      // $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row1){
                                                        if($teamid == $row1->team_id && $row1->over > 0.0){
                                                             $str .= '<p>' . $row1->wicket . '</p>';
                                                        }
                                                        $teamid = $row->team_id;
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>

                                                </div>
                                                </div>



                                                <div id="tab2a" class="tab">
                                                <h4>Batting</h4>
                                                  <div id="tab1a" class="tab active">
                                                   <div class="col-md-3 single-match-data">
                                                      <p class="nm-player">Player Name</p>
                                                      <?php
                                                      $str = ""; 
                                                      $second_team_id = "";                                                     
                                                      foreach($score_data as $row){
                                                        if($teamid == $row->team_id){
                                                          $str .= '<p>' . $row->p1_player_name . '</p>';
                                                          $second_team_id = $row->team_id;
                                                        }

                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>
                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Run</p>
                                                     <?php
                                                      $str = "";                                                      
                                                      foreach($score_data as $row){
                                                        if($teamid == $row->team_id){
                                                          $str .= '<p>' . $row->run . '</p>';
                                                        }
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>
                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Ball </p>
                                                    <?php
                                                      $str = "";                                                      
                                                      foreach($score_data as $row){
                                                        if($teamid == $row->team_id){
                                                          $str .= '<p>' . $row->ball . '</p>';
                                                        }
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>

                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Four </p>
                                                     <?php
                                                      $str = "";                                                      
                                                      foreach($score_data as $row){
                                                        if($teamid == $row->team_id){
                                                          $str .= '<p>' . $row->four . '</p>';
                                                        }
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>

                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Six </p>
                                                     <?php
                                                      $str = "";                                                      
                                                      foreach($score_data as $row){
                                                        if($teamid == $row->team_id){
                                                          $str .= '<p>' . $row->six . '</p>';
                                                        }
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>
                                                    <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Player Status </p>
                                                     <?php
                                                      $str = "";                                                      
                                                      foreach($score_data as $row){
                                                        if($teamid == $row->team_id){
                                                         $out_not = '';
                                                        if($row->out_not == '1'){ $out_not = 'Out' ; } else{ $out_not = 'Not Out'; }
                                                        $str .= '<p>' . $out_not . '</p>';
                                                        }
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>

                                                </div>
                                                    <div id="boll1" class="tab active">
                                                <h4>Bowlling</h4>
                                                   <div class="col-md-3 single-match-data">
                                                      <p class="nm-player">Player Name</p>
                                                      <?php
                                                      // $teamid = '0';
                                                      // $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row1){
                                                        if($second_team_id != $row1->team_id && $row1->over > 0.0){
                                                             $str .= '<p>' . $row1->p1_player_name . '</p>';
                                                        }
                                                        $teamid = $row->team_id;
                                                      }
                                                      echo $str;
                                                     ?>
                                                    
                                                   </div>
                                                   
                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Over</p>
                                                    <?php
                                                      // $teamid = '0';
                                                      // $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row1){
                                                        if($second_team_id != $row1->team_id && $row1->over > 0.0){
                                                             $str .= '<p>' . $row1->over . '</p>';
                                                        }
                                                        $teamid = $row->team_id;
                                                      }
                                                      echo $str;
                                                     ?>
                                                    
                                                   </div>


                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Run </p>
                                                    <?php
                                                      // $teamid = '0';
                                                      // $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row1){
                                                        if($second_team_id != $row1->team_id && $row1->over > 0.0){
                                                             $str .= '<p>' . $row1->given_run . '</p>';
                                                        }
                                                        $teamid = $row->team_id;
                                                      }
                                                      echo $str;
                                                     ?>
                                                    
                                                   </div>

                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Wide </p>
                                                    <?php
                                                      // $teamid = '0';
                                                      // $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row1){
                                                        if($second_team_id != $row1->team_id && $row1->over > 0.0){
                                                             $str .= '<p>' . $row1->wideball . '</p>';
                                                        }
                                                        $teamid = $row->team_id;
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>

                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Maiden </p>
                                                    <?php
                                                      // $teamid = '0';
                                                      // $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row1){
                                                        if($second_team_id != $row1->team_id && $row1->over > 0.0){
                                                             $str .= '<p>' . $row1->madian . '</p>';
                                                        }
                                                        $teamid = $row->team_id;
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>


                                                   <div class="col-md-1 single-match-data">
                                                     <p class="nm-player">Wicket </p>
                                                     <?php
                                                      // $teamid = '0';
                                                      // $i = 1;
                                                      $str = "";                                                      
                                                      foreach($score_data as $row1){
                                                        if($second_team_id != $row1->team_id && $row1->over > 0.0){
                                                             $str .= '<p>' . $row1->wicket . '</p>';
                                                        }
                                                        $teamid = $row->team_id;
                                                      }
                                                      echo $str;
                                                     ?>
                                                   </div>

                                                </div>
                                                   </div>
                                                </div>

                                            </div>
                                 
                                        </div>     
                                    </div>
                                </div>
                                 
                            </div>
                            </div> 
                   </div>
           </div><!--Close Top Match-->
        </section> 
@endsection

